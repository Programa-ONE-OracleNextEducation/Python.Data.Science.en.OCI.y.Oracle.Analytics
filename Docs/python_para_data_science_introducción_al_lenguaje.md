## **Realice este curso para Data Science y:**

- Escribe y ejecute tu primer código Python
- Aprenda los conceptos básicos del lenguaje como variables, funciones, listas, condicionales e importaciones
- Utilice Matplotlib para generar un gráfico en la práctica
- Desarrolle su lógica de programación de ciencia de datos
- Cree un notebook desde cero, siguiendo las principales convenciones del lenguaje y las buenas prácticas de programación

### **Aulas**

- **Variables, funciones y lenguaje de alto nivel**

  - Presentación
  - Preparando el ambiente
  - Variables
  - ¿Por qué usar Google Colab?
  - Mi primera función
  - Función nombre completo
  - Lenguaje de alto nivel
  - Lenguaje de alto nivel vs. Lenguaje de bajo nivel
  - Para saber más: Historia de Python
  - Haz lo que hicimos en aula: Variables y Función print()
  - Lo que aprendimos

- **Parámetros, condicionales y conversión de datos**

  - Proyecto del aula anterior
  - Función con parámetro
  - Una función para calcular la velocidad
  - Condicionales
  - Descubriendo el NPS de un curso
  - Conversión de tipos de datos
  - Permiso de conducir y TypeError
  - Para saber más: Operadores de Python
  - Haz lo que hicimos en aula: Parámetros de una función
  - Lo que aprendimos

- **Listas, bucles y tipo booleano**

  - Proyecto del aula anterior
  - Creando una lista
  - Eslogan de Alura
  - Bucles y Ciclos (Loops)
  - Descubriendo el NPS de varias notas de un curso
  - Tipo Booleano
  - ¿Cuál es el valor de a?
  - Diferentes tipos en una lista
  - Listas en Python
  - Para saber más: Nomenclatura de variables y tipo float
  - Haz lo que hicimos en aula: Listas y loops
  - Lo que aprendimos

- **El sistema de importación de librerías**

  - Proyecto del aula anterior
  - Importando una librería
  - Importando Bibliotecas y Módulos
  - Lista con números aleatorios
  - Tamaño de una lista
  - Para saber más: Proyecto Open Source
  - Haz lo que hicimos en aula: Importación de Bibliotecas y módulos
  - Lo que aprendimos

- **Graficando con matplotlib**

  - Proyecto del aula anterior
  - Creando un gráfico
  - Analizando gráficamente el desempeño escolar
  - Para saber más: Fue un excelente comienzo, pero esto continua...
  - Haz lo que hicimos en aula: Graficando con Matplotlib
  - Proyecto Final
  - Lo que aprendimos
  - Conclusión
