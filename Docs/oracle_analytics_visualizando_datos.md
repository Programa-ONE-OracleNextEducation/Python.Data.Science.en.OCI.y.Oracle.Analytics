## **Realice este curso para Data Visualization y:**

- Conozca las técnicas de visualización de datos
- Aprenda a trabajar con el software de Oracle Analytics
- Desarrolla el proceso de ETL dentro del software
- Utilice el recurso Augmented Analytics

### **Aulas**

- **Oracle Analytics**

  - Presentación
  - Conociendo el proyecto
  - La importancia de la visualización
  - Instalando Oracle Analytics
  - ¿Por dónde comenzar?
  - Para saber más: cómo utilizar Oracle Analytics en la nube
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Creando el proyecto**

  - Accediendo a los archivos del aula
  - Creando directorio de trabajo
  - Desafío: tipos de conexiones
  - Medidas atributos y tratamiento de datos
  - Tipos de datos
  - Para saber más: Augmented Analytics
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Creando gráficas**

  - Accediendo a los archivos del aula
  - Creando la primera visualización
  - Desafío: formas de crear visualizaciones
  - Recurso explicar columna
  - Sobre el recurso explicar
  - Gráfico de barras
  - Gráfico de barras apiladas
  - Para saber más: cuándo utilizar el gráfico de barras
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Interacción entre las visualizaciones**

  - Accediendo a los archivos del aula
  - Realizando los cálculos
  - Top 3 clientes
  - Trabajando con filtros
  - Series temporales
  - Gráfico de línea y tendencia
  - Visualización con mapas
  - ¿Cuál es el gráfico más adecuado?
  - Para saber más: mapas de calor
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Elaborando la presentación**

  - Construyendo la primera página
  - Layout del Canvas
  - Finalizando las métricas
  - COUNT y COUNT DISTINCT
  - Para saber más: importando visualizaciones externas
  - Storytelling
  - Para saber más: profundizando en el Storytelling
  - Haga lo que hicimos en aula
  - Proyecto final
  - Lo que aprendimos
  - Conclusión
