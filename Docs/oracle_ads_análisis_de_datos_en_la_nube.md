## **Realice este curso para Data Science y:**

- Entiende cómo trabajar con Data Science en la nube de Oracle
- Aprende cómo utilizar la biblioteca Advanced Data Science de Oracle
- Desarrolla proyectos de Data Science en el ambiente Oracle
- Coloca en práctica tus conocimientos sobre Data Science en la nube

### **Aulas**

- **Conociendo el ambiente Oracle**

  - Presentación
  - Free Tier
  - Para saber más: Free Tier
  - Creando un Stack
  - Ambiente de Data Science
  - Cargando un Kernel
  - Preparando el ambiente
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Conociendo los datos**

  - Proyecto del aula anterior
  - Archivos externos
  - ADS DataFrame
  - Cargando el CSV
  - Para saber más: diferentes tipos de DataFrame
  - Creando un ADS DataFrame
  - Para saber más: relación entre diabetes y glicemia
  - Tratamiento de datos faltantes
  - Desafío: rellenar las celdas de forma selectiva
  - Rellenando los valores NaN
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Trabajando con los datos**

  - Proyecto del aula anterior
  - Preparando los datos
  - Tipos de datos
  - Para saber más: tipos de datos
  - Ejecutando las transformaciones
  - Para saber más: transformando datos categóricos
  - Ejecutando transformaciones
  - Visualizando datos con ADS
  - Show in notebook
  - Generando más visualizaciones
  - Para saber más: análisis exploratorio
  - Desafío: grafica una columna
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Definiendo el alcance del proyecto**

  - Proyecto del aula anterior
  - Correlación
  - Para saber más: correlación
  - ADS y sugerencias automáticas
  - Recomendaciones
  - Aplicando sugerencias
  - Aplicando sugerencias al DataFrame
  - Comparando bases
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Preparando para ML**

  - Proyecto del aula anterior
  - Balanceo de datos
  - Para saber más: balanceo de datos
  - Técnicas de balanceo
  - Aplicando el balanceo
  - Documentación de datos
  - Para saber más: documentación de datos
  - Desafío: documenta tus conclusiones
  - Generando un CSV
  - Creando una base de datos
  - Haga lo que hicimos en aula
  - Proyecto final
  - Lo que aprendimos
  - Conclusión
  - Créditos
