## **Realice este curso para Data Science y:**

- Descubra cómo funcionan las tuplas y las listas
- Descubra la biblioteca de científicos de datos: Pandas
- Aprenda cómo encapsular código en funciones
- Use paquetes para organizar el código
- Trabajar con las Built-in Functions de Python

### **Aulas**

- **Presentación del curso**

  - Presentación
  - Preparando el ambiente
  - Instalación y ambiente de desarrollo
  - El lenguaje Python
  - Trabajando con datos
  - Primer contacto con Pandas
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Listas estáticas: Tuplas**

  - Proyecto del aula anterior
  - Conociendo las tuplas
  - Formas de crear una tupla
  - Selecciones en tuplas
  - Seleccionando elementos en tuplas
  - Iterando en tuplas
  - Bucles for con tuplas
  - La función zip()
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Mapeo de datos: Diccionarios**

  - Proyecto del aula anterior
  - Creando diccionarios
  - Definiciones de diccionarios
  - Forma simple de crear diccionarios
  - Operaciones con diccionarios
  - Operaciones básicas con diccionarios
  - Métodos de diccionarios
  - Conociendo los métodos de diccionarios
  - Iterando en diccionarios
  - Iterando en diccionarios de diccionarios
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Organización del código: Funciones y paquetes**

  - Proyecto del aula anterior
  - Built-in Functions
  - Fijando el conocimiento
  - Definiendo funciones con o sin parámetros
  - Kilometraje promedio de un vehículo
  - Definiendo funciones que devuelven valores
  - Mejorando nuestra función
  - Mejorando un poco más nuestra función
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **La biblioteca de los científicos de datos: Pandas**

  - Proyecto del aula anterior
  - Estructuras de datos
  - Pandas y sus estructuras de datos
  - Creando DataFrames
  - Selecciones con DataFrames
  - Particiones con DataFrames
  - Utilizando .loc y .iloc para selecciones
  - Query con DataFrames
  - Realizando consultas en un DataFrame
  - Iterando con DataFrames
  - Para saber más: Formas de iterar en un DataFrame
  - Tratamiento de datos
  - Identificando y tratando datos faltantes
  - Haz lo que hicimos en el aula
  - Proyecto Final
  - Lo que aprendimos
  - Conclusión
