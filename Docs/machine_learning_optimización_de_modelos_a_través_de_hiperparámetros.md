## **Realice este curso para Machine Learning y:**

- Entiende que son os hiperparámetros y los espacios de parámetros
- Explora los espacios de forma determinística
- Optimiza tus modelos
- Evita el overfit
- Implementa la exploración desde la base
- Aprende a utilizar y explorar con el GridSearchCV
- Nested cross validation

### **Aulas**

- **Hiper parámetros, optimización y overfit**

  - Presentación
  - Preparando el ambiente
  - Entendiendo los parámetros
  - Los parámetros
  - ¿Cuanto más complejo el árbol, mejor?
  - Optimización de un hiper parámetro y el problema de overfit
  - Definición de max_depth
  - Revisión de la exploración de un espacio de parámetros
  - Haga lo que hicimos
  - Lo que aprendimos

- **Explorando 2 dimensiones de hiper parámetros**

  - Proyecto del aula anterior
  - Espacio de parámetros de dos dimensiones
  - Explorando hiper parámetros de más dimensiones
  - Matriz de correlación y explorando más espacios de parámetros
  - Correlación
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Trabajando con 3 ó más dimensiones**

  - Proyecto del aula anterior
  - Explorando 3 ó más hiper parámetros
  - Explorando hiper parámetros
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Búsqueda de hiper parámetros con GridSearchCV**

  - Proyecto del aula anterior
  - Utilizando GridSearchCV
  - Buscando datos
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Nested Cross Validation y validando el modelo elegido**

  - Proyecto del aula anterior
  - Nested Cross Validation y validando el mejor modelo
  - Validación
  - Haga lo que hicimos en aula
  - Proyecto Final
  - Lo que aprendimos
  - Conclusión
