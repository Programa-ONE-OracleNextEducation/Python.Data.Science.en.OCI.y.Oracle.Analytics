## **Realice este curso para Machine Learning y:**

- Utilice un Kernel de Machine Learning en el ambiente Oracle Cloud
- Use Oracle ADS para realizar la lectura de datos
- Cree modelos de Machine Learning en el ambiente Oracle Cloud.
- Explore modelos con Oracle ADS AutoML
- Valide modelos con Oracle ADS
- Explique modelos con o Oracle ADS
- Publique modelos en formato de API em el ambiente Oracle Cloud

### **Aulas**

- **Creando el modelo en Oracle Cloud**

  - Presentación
  - Para saber más: Free Tier
  - Preparando el ambiente
  - Creando el Jupyter NB
  - Función de un Kernel
  - Lectura de la base de datos
  - Importancia del balanceo de datos
  - Creando nuestro primer modelo
  - Para saber más: ¿Qué es un árbol de decisión?
  - Mejorando el primer modelo
  - Para saber más: ¿Cómo funciona la matriz de confusión?
  - Selección manual de features
  - Para saber más: ¿Qué son los hiperparámetros?
  - Haga lo que hicimos
  - Lo que aprendimos

- **Explorando Oracle AutoML**

  - Proyecto del aula anterior
  - Explorando Oracle AutoML
  - ¿Qué son modelo_automl y baseline?
  - Modelos generados por AutoML
  - Para saber más: Grid Search y Random Search
  - Para saber más: Random Forest
  - Features y modelo seleccionado
  - Lectura de la tabla con show_in_notebook()
  - Comparando los modelos
  - Haga lo que hicimos
  - Lo que aprendimos

- **Evaluando el modelo**

  - Proyecto del aula anterior
  - ADS Evaluator
  - Para saber más: Precision Recall x Curva ROC
  - Analizando las métricas
  - Para saber más: Gráfico de Gain x Lift
  - Evaluando más métricas
  - Para saber más: Overfitting
  - Overfitting
  - Desafío: explorando parámetros de AutoML
  - Haga lo que hicimos
  - Lo que aprendimos

- **Entendiendo el modelo**

  - Proyecto del aula anterior
  - Model Explainability
  - Valores de Features x Clasificación
  - Gráfico PDP
  - Explicando decisión individual
  - Gobernanza y Leyes de protección de datos
  - Explicación global y local
  - Haga lo que hicimos
  - Lo que aprendimos

- **Publicando el modelo**

  - Proyecto del aula anterior
  - Creando nuestro artefacto
  - Para saber más: explorando los archivos del Artefacto
  - Catálogo de modelos
  - Para saber más: utilizando las variables de entorno
  - Importando modelo del catálogo
  - API del modelo
  - Ventajas del catálogo de modelos
  - Haga lo que hicimos
  - Proyecto Final
  - Lo que aprendimos
  - Conclusión
  - Créditos
