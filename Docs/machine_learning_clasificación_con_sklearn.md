## **Realice este curso para Machine Learning y:**

- Aplique machine learning en su empresa
- Practique con diversos ejemplos
- Vea como los algoritmos de clasificación están por todos lados en nuestro dia a dia
- Analize resultados con la mente de um científico de datos
- Compare el resultado de algoritmos lineales y lineales
- Entienda a profundidad lo que es aprendizaje de máquina para clasificación
- Haga estudios replicables, con estrategias de prueba y testeo
- Vea Support Vector Machines, Árboles de Decisión y Dummy Classifiers

### **Aulas**

- **Introducción a la clasificación**

  - Presentación
  - Preparando el ambiente
  - El primer proyecto
  - Entrenamiento y prueba de un modelo de clasificación
  - Estandarización de nombres
  - Practicando
  - Haga lo que hicimos
  - Lo que aprendimos en el aula

- **Lectura y manipulación de datos**

  - Proyecto del aula anterior
  - Preparando el ambiente
  - Lectura y manipulación de datos
  - Estratificando splits
  - Haga lo que hicimos
  - Renombrando
  - Lo que aprendimos en el aula

- **Modelo baseline**

  - Proyecto del aula anterior
  - Preparando el ambiente
  - Probando en dos dimensiones
  - Curva de decisión
  - Haga lo que hicimos
  - Algoritmo base
  - Lo que aprendimos en el aula

- **Estimadores no lineales**

  - Proyecto del aula anterior
  - Estimadores no lineales
  - Linealidad vs no linealidad
  - Haga lo que hicimos
  - Lo que aprendimos en el aula

- **Dummy classifiers y Árboles de decisión**

  - Proyecto del aula anterior
  - Preparando el ambiente
  - Trabajando en un nuevo proyecto
  - Dummy classifier y SVC
  - Árboles de decisión y visualización de reglas de decisión
  - Transformaciones
  - Haga lo que hicimos
  - Proyecto final
  - Lo que aprendimos en el aula
  - Conclusión
