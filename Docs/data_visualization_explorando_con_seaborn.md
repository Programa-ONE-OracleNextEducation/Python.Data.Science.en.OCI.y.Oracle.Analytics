## **Realice este curso para Data Visualization y:**

- Importe datos y trabaje con dataframes
- Aprenda a tratar los datos antes de analizalos
- Sepa como utilizar Seaborn para creación de diferentes gráficos
- Utilize Python Pandas con la incrível herramienta de Google Colaboratory
- Desarrolle técnicas graficas y cuantitativas, buscando a obtener informaciones relevantes

### **Aulas**

- **Importando y traduciendo variables**

  - Presentación
  - Preparando el ambiente
  - Importando base de datos
  - Leyendo un archivo CSV
  - Traduciendo columnas e variables
  - Traducción de variables
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Análisis exploratoria de variables**

  - Proyecto del aula anterior
  - Importando Seaborn y análisis de distribución
  - Creando un gráfico de distribución
  - Creando variable numérica
  - Insertando nuevos campos
  - Parámetros de gráficos
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Comparando y analizando variables categóricas**

  - Proyecto del aula anterior
  - Countplot, catplot y swarmplot
  - Generando gráficos con Seaborn
  - Boxplot y Violinplot
  - Creando y analizando variable categórica
  - Agrupando el índice de uso
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Análisis de variables numéricas y regresión**

  - Proyecto del aula anterior
  - Displot
  - Analizando distribuciones
  - Scatterplot y lmplot
  - Test de hipótesis
  - Sobre el test de hipótesis
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Análisis conjunta de variables**

  - Proyecto del aula anterior
  - Jointplot
  - Comparando la edad con el índice de uso
  - Pairplot
  - Entendiendo un Pairplot
  - Haga lo que hicimos en aula
  - Proyecto Final
  - Lo que aprendimos
  - Conclusión
