## **Realice este curso para Data Science y:**

- Aprenda a importar datos y exportar dataframes
- Domina como limpiar y tratar datos faltantes
- Conoce como remover outliers y crear nuevas variables
- Seleccione y genera las frecuencias de los datos
- Entienda como usar Python Pandas, Matplotlib, Jupyter y Anaconda

### **Aulas**

- **Presentación del curso**

  - Presentación
  - Preparando el ambiente
  - Instalación y ambiente de desarrollo
  - El lenguaje Python
  - Trabajando con datos
  - Primer contacto con Pandas
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Importando datos**

  - Importando la base
  - Conociendo Colab
  - Conociendo la base de datos
  - Informaciones de un Dataframe
  - (Extra) Importando otras fuentes
  - Funciones para obtención de datos
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Series y Index**

  - Proyecto del aula anterior
  - Eliminando valores repetidos
  - Utilizando métodos en Pandas
  - Redefiniendo el index
  - Índice de las líneas de un Dataframe
  - (Extra) Creando dataframes
  - Formas de creación de un Dataframe
  - (Extra) Concatenando Dataframes
  - Conociendo mejor las estructuras de datos
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Filtrando datos**

  - Proyecto del aula anterior
  - Inmuebles residenciales
  - Determinando selecciones
  - Exportando base de datos
  - Revisando lo que aprendimos
  - (Extra) Organizando dataframes
  - El funcionamiento de sort_index
  - Más sobre clasificaciones
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Frecuencias de inmuebles**

  - Proyecto del aula anterior
  - Selecciones y frecuencias
  - Dataframe para los próximos ejercicios
  - Selección (nivel 1)
  - Selección (nivel 2)
  - Selección (nivel 3)
  - (Extra) Formas de selecciones
  - Selección (nivel 4)
  - Selección (nivel 5)
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Tratando los datos faltantes**

  - Proyecto del aula anterior
  - Excluyendo valores nulos
  - Conocimientos básicos sobre missing values
  - Tratamiento condicional
  - Revisando el proceso
  - (Extra) Métodos de interpolación
  - Llenando datos faltantes
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Nuevas variables**

  - Proyecto del aula anterior
  - Creando nuevas variables
  - Identifique el error al crear variables
  - Excluyendo variables
  - Métodos de exclusión de variables
  - (Extra) Contadores
  - Probando monedas
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Estadísticas descriptivas**

  - Proyecto del aula anterior
  - Creando agrupamientos
  - Sumando informaciones
  - Estadísticas descriptivas
  - Seleccionando las estadísticas descriptivas
  - (Extra) Creando rangos de valor
  - Conociendo la función cut()
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Excluyendo Outliers**

  - Proyecto del aula anterior
  - Identificando y Excluyendo Outliers
  - Estadísticas del box plot
  - Identificando y Excluyendo Outliers por Grupo
  - Sobre el box plot
  - (Extra) Más sobre gráficos
  - La biblioteca matplotlib
  - Haga lo que hicimos en aula
  - Proyecto del curso
  - Lo que aprendimos
  - Conclusión
