# **Python, Data Science en OCI y Oracle Analytics**

## **Python para Data Science**

Con el avance de la tecnología, la cantidad de datos disponibles tuvo una explosión exponencial. Es así que el área de **Ciencia de Datos** ganó relevancia para auxiliar empresas y profesionales que necesitan realizar análisis avanzados para tomar mejores decisiones. En la actualidad la principal herramienta utilizada por científicos de datos en todo el mundo es el lenguaje **Python**, con su simplicidad, versatilidad y crecimiento en diversos segmentos, Python se ha consolidado como una de los principales lenguajes de programación en la actualidad.

- **¿Por qué aprender Python?**

El lenguaje **Python está entre los 5 lenguajes más populares** en el mundo, según una encuesta de RedMonk. Esa popularidad se debe principalmente por su característica de exigir pocas líneas de código y permitir una lectura fácil. También es importante destacar su hegemonía cuando el asunto se trata de Ciencia de Datos. En el diverso y extenso mundo de datos, **Python se destaca** en diversos frentes como ser: análisis de datos, computación gráfica, procesamiento de Big Data, computación científica y en las técnicas de **Machine Learning** y consecuentemente en **Deep Learning**.

- **¿Qué es lo que vas aprender en esta formación?**

En esta formación vas a aprender de una forma práctica y didáctica la versión 3 de Python y lo principal con un **enfoque total en Ciencia de Datos**. Si tienes poco o nada de conocimiento en programación, no hay problema, preparamos cursos para aquellos que están dando sus **primeros pasos** con el lenguaje. Y claro también en esta formación encontrarás contenido más avanzado donde podrás crear tu primer modelo de **Machine Learning**, vas a conocer las principales bibliotecas para el área de ciencia de datos como ser: Pandas, Matplotlib, Seaborn y SKLearn y tener contacto con diversas frentes del área de ciencia de datos.

También tendrás la oportunidad de conocer y profundizar en los **Servicios para Análisis de Datos en la Nube Oracle** y en el software **Oracle Analytics**, herramientas Oracle para trabajar con datos.

Si tu deseo es trabajar con datos y volverte un **científico de datos**, esta formación es tu primer paso para prepararte para otros tópicos más avanzados de la profesión.

¿Qué dices?, ¿Vamos juntos?.

Esta formación forma parte del Programa ONE, una alianza entre Alura Latam y Oracle.

## **De quien vas a aprender**

- Mauricio Loaiza
- Alejandro Gamarra
- Álvaro Hernando Camacho Diaz
- Christian Velasco

## **Paso a paso**

### **1. Conociendo el lenguaje y las principales librerías**

Da tus primeros pasos con el lenguaje, aprende lógica de programación en la práctica y realiza tus primeros análisis con datos reales. Conoce como Python puede ser útil en el día a día de un profesional que trabaja con análisis o planificación.

En esta primera parte, irás a conocer Python y sus principales aspectos, enfocando en ciencia de datos. Vas a comenzar con un simple Hello World y a medida que vas avanzando irás sumergiéndote en la biblioteca más famosa para análisis de datos: Pandas.

[Python para Data Science: Introducción al Lenguaje](https://app.aluracursos.com/course/python-data-science-introduccion "Python para Data Science: Introducción al Lenguaje"). Contenido [aqui](./Docs/python_para_data_science_introducción_al_lenguaje.md).

[Python para Data Science: Funciones, Librerías y Pandas básico](https://app.aluracursos.com/course/python-data-science-funciones-librerias-pandas-basico "Python para Data Science: Funciones, Librerías y Pandas básico"). Contenido [aqui](./Docs/python_para_data_science_funciones_librerías_y_pandas_básico.md).

[¿Por qué estudiar Data Science?](https://www.youtube.com/watch?v=7_UXo-aqAeM "YouTube").

[Clasificando texto con Python](https://www.aluracursos.com/blog/clasificando-texto-con-python "Alura LATAM").

[¿Cómo comparar objetos en Python?](https://www.aluracursos.com/blog/como-comparar-objetos-en-python "Alura LATAM").

[Buscando tweets con Python](https://www.aluracursos.com/blog/buscando-tweets-con-python "Alura LATAM").

### **2. Avanzando en Pandas**

En este bloque continúa profundizando tus conocimientos en la **popular biblioteca de Pandas**. Verás que provee herramientas poderosas para análisis de datos.

[Python Pandas: Tratamiento y análisis de datos](https://app.aluracursos.com/course/python-pandas-tratamiento-analisis-datos "Python Pandas: Tratamiento y análisis de datos"). Contenido [aqui](./Docs/python_pandas_tratamiento_y_análisis_de_datos.md).

[Manipulando datos gigantes con Pandas](https://www.aluracursos.com/blog/manipulando-datos-gigantes-con-pandas "Alura LATAM").

[Cómo eliminar filas y columnas con Pandas en Python](https://www.aluracursos.com/blog/como-eliminar-filas-y-columnas-en-pandas "Alura LATAM").

### **3. Visualización de Datos**

Ahora que ya conoces mejor la biblioteca Pandas, ya estás apto para realizar procesos de transformación y análisis de datos. El siguiente paso es aprender a crear gráficos que expresen y comuniquen mejor los insights encontrados. El enfoque en este bloque es trabajar mejor las visualización y para ello vamos a estudiar 2 bibliotecas especiales para ese fin: **Seaborn**.

[Data Visualization: Explorando con Seaborn](https://app.aluracursos.com/course/data-visualization-seaborn "Data Visualization: Explorando con Seaborn"). Contenido [aqui](./Docs/data_visualization_explorando_con_seaborn.md).

### **4. Machine Learning**

Llegó la hora de tener tu primer contacto con las famosas técnicas de **Machine Learning**, irás a conocer diversos problemas del día a día que pueden ser solucionados con aprendizaje de máquina.

Aprende como puedes ayudar a tu empresa o negocio a crescer, por ejemplo, clasificando clientes en categorías que luego pueden servir como indicador de productos a ser ofrecidos. Analiza resultados, compara diferentes algoritmos de una manera válida y usa el poder del Machine Learning para optimizar tus procesos.

Descubre como mejorar la eficiencia de nuestros algoritmos com el GridSearchCV y como validar el modelo seleccionado con técnicas de cross validation.

¡Este módulo está repleto de desafios, por lo tanto, prepárate para sumergir a fondo!

[Live Café Punto Tech - ¿Cómo dar tus primeros pasos en el mundo de Data Science?](https://www.youtube.com/watch?v=lEfMXSX4mz4&t=1603s "YouTube").

[Machine Learning: clasificación con SKLearn](https://app.aluracursos.com/course/machine-learning-clasificacion-sklearn "Machine Learning: clasificación con SKLearn"). Contenido [aqui](./Docs/machine_learning_clasificación_con_sklearn.md).

[Primeros pasos en Inteligencia Artificial (IA)](https://www.aluracursos.com/blog/primeros-pasos-en-inteligencia-artificial-ia "Alura LATAM").

[Machine Learning: Optimización de modelos a través de hiperparámetros](https://app.aluracursos.com/course/machine-learning-optimizacion-modelos-hiperparametros "Machine Learning: Optimización de modelos a través de hiperparámetros"). Contenido [aqui](./Docs/machine_learning_optimización_de_modelos_a_través_de_hiperparámetros.md).

### **5. Ciencia de Datos en la Nube Oracle y Oracle Analytics**

Ahora es momento de conocer y profundizar en los **Servicios para Análisis de Datos en la Nube Oracle**, a través de la biblioteca **ADS (Advanced Data Science)** podrás desarrollar proyectos de análisis de datos en un ambiente Oracle.

También aprenderás a utilizar **Oracle Analytics**, una herramienta para desktop que permite realizar análisis y construir dashboards y gráficos para una correcta visualización de datos.

[Oracle ADS: análisis de datos en la nube](https://app.aluracursos.com/course/oracle-ads-analisis-datos-nube "Oracle ADS: análisis de datos en la nube"). Contenido [aqui](./Docs/oracle_ads_análisis_de_datos_en_la_nube.md).

[Machine Learning con Oracle ADS: productividad en la creación de modelos](https://app.aluracursos.com/course/oracle-ads-productividad-creacion-modelos "Machine Learning con Oracle ADS: productividad en la creación de modelos"). Contenido [aqui](./Docs/machine_learning_con_oracle_ads_productividad_en_la_creación_de_modelos.md).

[Oracle Analytics: visualizando datos](https://app.aluracursos.com/course/oracle-analytics-visualizando-datos "Oracle Analytics: visualizando datos"). Contenido [aqui](./Docs/oracle_analytics_visualizando_datos.md).

### **6. Introducción a la formación**

[Introducción a Formación de Python para Data Science](https://www.youtube.com/watch?v=-QL8wcrc5mc "YouTube").

[Uso Del Foro](https://www.youtube.com/watch?v=ZhXdFO6SxQ4 "YouTube").
