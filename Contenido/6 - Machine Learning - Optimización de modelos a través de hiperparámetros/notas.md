# **Machine Learning - Optimización de modelos a través de hiperparámetros**

## **Hiper parámetros, optimización y overfit**

### **Preparando el ambiente**

En este curso utilizaremos Google Colab para escribir nuestro código Python y ejecutar los ejercicios. Para esto, necesitarás una cuenta de Google y acceder a [este enlace](https://colab.research.google.com/ "Google Colab").

### **Definición de max_depth**

José está estudiando Machine Learning en Alura. Durante su estudio llegó a la parte de los hiper parámetros, donde conoció y vio ejemplos de cómo son utilizados.

Pero con eso, surgió una pregunta con respecto al hiper parámetro max_depth: ¿Para qué se utiliza?

Rta.

Se utiliza para definir el tamaño del árbol de decisión. Cuando un valor es colocado en max_depth, el tamaño del árbol se establece a partir de este valor.

### **Lo que aprendimos**

- A definir y utilizar **max_depth**;
- A definir sobre un árbol de decisión (**DecisionTreeClassifier**);
- A mostrar gráficos con seaborn;
- A mostrar gráficos con matplotlib;
- A definir hiper parámetros.

---

## **Explorando 2 dimensiones de hiper parámetros**

### **Correlación**

Mayra es una analista de datos que brinda servicios a empresas. Recientemente ella accedió a una hoja de cálculo de una empresa de automóviles que contenía algunos datos:

|     | precio    | vendido | edad_años | km_uso    | modelo |
| --- | --------- | ------- | --------- | --------- | ------ |
| 0   | 30941.02  | 1       | 18        | 35085.221 | 18     |
| 1   | 40557.96  | 1       | 20        | 12622.053 | 24     |
| 2   | 89627.50  | 0       | 12        | 11440.798 | 14     |
| 3   | 95276.14  | 0       | 3         | 43167.326 | 6      |
| 4   | 117384.68 | 1       | 4         | 12770.112 | 5      |

A partir de esta hoja de cálculo, a esta empresa le gustaría que ella hiciera una correlación de datos utilizando pandas.

¿Cómo funciona una correlación de datos con pandas?

Rta.

Si los valores crecen juntos, ellos pueden tener una correlación. Usemos el ejemplo de un automóvil que tiene un año de fabricación y un kilometraje recorrido, cuanto mayor sea el tiempo de uso, mayor será el kilometraje recorrido, los valores están correlacionados.

### **Lo que aprendimos**

- A definir los elementos mínimos en un árbol de decisión;
- A utilizar **min_samples_leaf** para el entrenamiento;
- Qué es **corr** de pandas.

---

## **Trabajando con 3 ó más dimensiones**

### **Explorando hiper parámetros**

Según lo que fue mostrado, ¿bajo qué circunstancias es mejor usar hiper parámetros?

Rta.

Cuando queremos explorar el espacio de varias dimensiones, donde podremos definir los hiper parámetros que nos gustaría probar. Podemos explorar diferentes dimensiones de datos, es decir, podemos definir tantos hiper parámetros como sea necesario.

### **Lo que aprendimos**

- A utilizar más de un hiper parámetro;
- A generar un gráfico con matplotlib;
- Qué es **min_samples_split**.

---

## **Búsqueda de hiper parámetros con GridSearchCV**

### **Buscando datos**

Anderson está desarrollando una aplicación con análisis de datos. Él ha creado un cuadro de datos (dataframe) que maximiza los hiper parámetros, pero le gustaría hacer una búsqueda en forma de red (grid) para facilitar su análisis.

¿Qué método puede utilizar?

Rta.

GridSearch para optimizar y encontrar el mejor conjunto de hiper parámetros. GridSearch busca dentro del dataframe e incrementa su búsqueda en forma de red (grid).

### **Lo que aprendimos**

- A utilizar **GridSearchCV**;
- A utilizar **accuracy_score**.

---

## **Nested Cross Validation y validando el modelo elegido**

### **Validación**

Como se demostró durante la clase, ¿cómo funciona Nested Cross Validation?

Rta.

Se utiliza cuando necesitamos buscar hiper parámetros para realizar una nueva validación. Con los hiper parámetros encontrados establecemos los parámetros deseados y hacemos la validación utilizando Nested Cross Validation. Por otro lado, cuando queremos separar en grupos usamos GroupKFold y un Cross Validation para hacer esto.

### **Lo que aprendimos**

- Qué es **Nested Cross Validation**;
- A importar **GroupKFold**;
- A visualizar el mejor estimador.
