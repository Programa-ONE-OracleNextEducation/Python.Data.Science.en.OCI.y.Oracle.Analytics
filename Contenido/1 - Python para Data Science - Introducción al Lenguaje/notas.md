# **Python para Data Science - Introducción al Lenguaje**

## **Variables, funciones y lenguaje de alto nivel**

### **Preparando el ambiente**

**¡Hola!**

Mi nombre es Christian Velasco y es un placer darte la bienvenida a este curso de Python para Data Science: Introducción al Lenguaje.

Espero que sea una experiencia increíble de aprendizaje y que podamos vencer todos los desafíos juntos. En esta aula, vamos a dar nuestros primeros pasos en el lenguaje Python, aprendiendo sus fundamentos, algunas buenas prácticas y principales convenciones.

**Ambiente de Análisis**

Utilizaremos una herramienta de Google llamada Colaboratory, que es similar a Jupyter Notebook y no requiere configuración para su uso.

**Google Colaboratory**

Para usar este ambiente, debes tener una cuenta de Gmail, ya que cada Notebook virtual se almacenará en Google Drive. Si no tienes una cuenta de Gmail, créala haciendo clic en este enlace.

[Crear cuenta Gmail](https://accounts.google.com/signup/v2/webcreateaccount?flowName=GlifWebSignIn&flowEntry=SignUp "GMail")

Información importante sobre Colaboratory

- El código de tu Notebook se ejecuta en una máquina virtual dedicada a tu cuenta. Las máquinas virtuales se reciclan después de un cierto tiempo de inactividad, o si la ventana está cerrada.
- Para restaurar tu Notebook, es posible que debas cargar el archivo .csv y ejecutar las opciones "Tiempo de ejecución" y "Reiniciar y ejecutar todas ...".
- Para saber más sobre Colab, creamos un [artículo en Alura](https://www.aluracursos.com/blog/google-colab-que-es-y-como-usarlo "Alura LATAM") explicando a detalle cómo funciona la herramienta.

En caso de dudas durante el curso, cuenta siempre con el Foro de Alura, si no tuvieses ninguna duda, ¿qué tal ayudar a alguien en el Foro compartiendo conocimiento y experiencias?

### **¿Por qué usar Google Colab?**

En este curso vamos a usar Google Colab como ambiente de trabajo y desarrollo. ¿Cuál de las siguientes afirmaciones son correctas en relación a esta poderosa herramienta de trabajo?

1. Es un ambiente colaborativo que te permite trabajar con Python a través de un Notebook.
2. Se requiere de una licencia premium paga para acceder a los recursos completos de la herramienta.
3. No requiere configuración y permite compartir contenido fácilmente.
4. Lo único que necesitas es una cuenta de Gmail para usar Google Colab.

Rta.

Solo la segunda alternativa 2 es incorrecta La alternativa 2 es falsa ya que no se requiere una licencia premium paga para acceder a los recursos de Colab. Es totalmente gratis.

**Sabiendo esto, ¡empecemos!**

### **Función nombre completo**

En Python existen funciones integradas llamadas [funciones built-in](https://docs.python.org/es/3/library/functions.html "Functions"), por ejemplo, la función print() que usamos.

Además, es posible crear nuestras propias funciones para realizar una determinada tarea, como ilustra el siguiente código:

```py
def nombre_completo():
   primer_nombre = input('¿Cuál es tu primer nombre? ')
   apellido = input('¿Cuál es tu apellido? ')
   nombre_completo = primer_nombre + ' ' + apellido
   print(nombre_completo)
nombre_completo()
```

Al ejecutar el código de arriba, podemos afirmar que:

Rta.

- Se mostrarán dos cajas de textos preguntando nombre y apellido. El valor del nombre será almacenado en la variable **primer_nombre** y el valor de apellido en la variable **apellido**.
- La variable **nombre_completo** será la concatenación de las variables **primer_nombre** y **apellido**, con un espacio entre ellas. En python, al utilizar el operador suma entre palabras, concatenamos (o unimos) el contenido.

### **Lenguaje de alto nivel vs. Lenguaje de bajo nivel**

Como vimos en aula, la [descripción de Python en Wikipedia](https://en.wikipedia.org/wiki/Python_%28programming_language%29 "Wikipedia") es:

**Python es un lenguaje de programación de alto nivel...**

Sabiendo esto, analiza las afirmaciones sobre lenguajes de alto y bajo nivel, y marca las correctas.

Rta.

- El lenguaje de bajo nivel está más cerca del lenguaje de máquina. Como vimos en el aula, comparando el ejemplo de Assembly y de Python, el lenguaje de bajo nivel es menos parecido al lenguaje humano.
- Los lenguajes de programación de alto nivel como Python, poseen un nivel de abstracción relativamente elevado, podemos decir, por lo tanto, que son más cercanos al lenguaje humano. Los lenguajes de alto nivel son más parecidos o similares a los lenguajes humanos.
- La principal ventaja de los lenguajes de programación de bajo nivel es el mejor uso y rendimiento de la máquina. Como el código de bajo nivel está escrito en lenguaje de máquina, existe una ganancia significativa en el rendimiento de la máquina.

**Para saber más: Historia de Python**

Python es un lenguaje de programación interpretado, orientado a objetos de alto nivel y con semántica dinámica.

La simplicidad de Python reduce el mantenimiento de un programa. Python soporta bibliotecas y módulos (contenidos que veremos a lo largo del curso), lo que fomenta la programación modular y la reutilización del código.

Es uno de los lenguajes que más ha crecido debido a su compatibilidad (se ejecuta en la mayoría de los sistemas operativos) y la capacidad de soportar otros lenguajes. Aplicaciones que vemos a diario como Dropbox, Reddit e Instagram están escritas en Python.

Python también es el lenguaje más popular para análisis de datos y conquistó a la comunidad científica.

**Breve historia del lenguaje**

Python fue creado en 1990 por [Guido Van Rossum](https://es.wikipedia.org/wiki/Guido_van_Rossum "Wikipedia") en el centro de Matemática Stichting en Holanda como sucesor del lenguaje [ABC](https://es.wikipedia.org/wiki/ABC_%28lenguaje_de_programaci%C3%B3n%29 "Wikipedia"). Guido es recordado como el principal autor de Python, pero también otros programadores ayudaron con muchas contribuciones.

El lenguaje ABC fue diseñado para ser utilizado por "no programadores", pero al inicio mostró ciertas limitaciones y restricciones. El mayor reclamo de los primeros estudiantes "no programadores" de este lenguaje, era la presencia de reglas arbitrarias que los lenguajes de programación habían establecido tradicionalmente, muchas cosas de bajo nivel todavía se hacían y eso no agradaba al público.

A raíz de eso, Guido se propuso la tarea de crear un lenguaje de script simple que contenga algunas de las mejores propiedades de ABC. Mejoró el lenguaje ABC e incorporó las listas Python, diccionarios, declaraciones básicas y uso obligatorio de sangría, todo esto diferencia Python del lenguaje ABC.

Probablemente ya escuchaste o leíste en algún lugar que Python es un lenguaje interpretado o un lenguaje de script. En cierto sentido, también es cierto que Python es tanto un lenguaje interpretado como un lenguaje compilado. Un compilador traduce lenguaje Python a lenguaje de máquina. El código Python es traducido en un código intermediario que debe ser ejecutado por una máquina virtual conocida como PVM (Python Virtual Machine).

El intérprete hace la traducción en tiempo real para el código de máquina, es decir, en tiempo de ejecución. El compilador traduce todo el programa en código de máquina de una sola vez y luego lo ejecuta, creando un archivo que se puede ejecutar (ejecutable). El compilador genera un informe de errores (en caso existan) y el intérprete detiene la traducción cuando encuentra un primer error.

**¿Más información sobre Python?**

Para más información, accede a la [documentación oficial de Python en este link](https://docs.python.org/es/3/ "Documentation") o descarga el [Ebook de Python para Data Science](https://caelum-online-public.s3.amazonaws.com/ESP-DS-24-alura-latam-apostilla-python-para-DS/ds-24-ebook-alura-latam-python-para-data-science-vbf.pdf) que construimos en Alura para ayudarte en esta jornada de aprendizaje.

---

## **Parámetros, condicionales y conversión de datos**

### **Una función para calcular la velocidad**

Una persona va a la panadería todos los días. La distancia de su casa hasta el lugar es de 100 metros y todo el recorrido se realiza en 20 segundos.

Para calcular la velocidad promedio y encontrar la relación entre el espacio y el tiempo, podemos dividir el espacio recorrido por el tiempo.

Sabiendo esto, la persona desea crear una función llamada **velocidad**, que reciba **2** parámetros llamados _espacio_ y _tiempo_, que realice el cálculo respectivo y que muestre una salida semejante al siguiente ejemplo:

```txt
Velocidad: 5 m/s
```

¿Cuál de las siguientes alternativas describe esta función?

Rta.

```py
def velocidad(espacio, tiempo):
 v = espacio / tiempo
 print(f ' Velocidad: {v} m/s')

velocidad(100, 20)
```

El nombre de la función se definió como se esperaba y la función toma 2 argumentos. Además, la salida de la función se muestra de acuerdo con lo requerido.

### **Descubriendo el NPS de un curso**

Las sentencias condicionales son recursos que nos ayudan a ejecutar acciones donde tenemos diferentes opciones o caminos que seguir en función a reglas o condiciones. Evalúa la siguiente expresión condicional hecha en Python para descubrir cómo se interpreta la nota de NPS (Net Promoter Score) dada por un alumno a un curso de Alura.

```py
def clasificar_nps(nota):
  if nota <= 6:
    print('El alumno es considerado Detractor')
  else:
    if nota <= 8:
      print('El alumno es considerado Neutro')
    else:
      print('El alumno es considerado Promotor')

clasificar_nps(9)
```

Selecciona el resultado correcto generado por la función.

Rta.

El alumno es considerado Promotor. La nota 9 es clasificada como Promotor en la secuencia condicional del enunciado.

### **Permiso de conducir y TypeError**

Un científico de datos creó la siguiente función, para saber cuántos años le faltan a una persona para obtener su permiso de conducir. En caso de tener la edad permitida, un mensaje informará a la persona que ya puede obtenerlo.

```py
def permiso_de_conducir():
 edad = input('¿Cuál es tu edad? ')
 if edad >= 18:
   print('Ya tienes edad suficiente para obtener permiso de conducir')
 else:
   tiempo = 18 - edad
   print(f 'Tranquilo, faltan {tiempo} año(s) para obtener permiso de conducir')

permiso_de_conducir()
```

Al ejecutar la función, recibes el siguiente mensaje de error:

```py
TypeError: '>=' not supported between instances of 'str' and 'int'
```

Rta.

- Para solucionar el problema, antes de realizar la comparación, es necesario convertir el tipo str de la edad a int. Esta conversión puede ser realizada a través del código: edad = int(edad).
- El error se produce porque el contenido de la variable edad es str. Observa en el error que, la expresión matemática >= no es compatible con instancias de tipo str y int.

Sabiendo esto, analiza las siguientes afirmaciones y marca aquellas que puedan ayudar a este científico de datos:

---

## **Listas, bucles y tipo booleano**

### **Eslogan de Alura**

La plataforma de Alura cuenta con diversos cursos de tecnología y negocios. Podemos afirmar que la plataforma tiene:

Cursos de Tecnología y Negocios Digitales

Sabiendo esto, observa el siguiente código con todas las palabras en una lista fuera de orden:

```py
frase_alura = [' Digitales', 'y ', 'Negocios ', 'de ', 'Tecnología ', 'Cursos ']
```

Sabiendo esto, analiza las siguientes alternativas y marca la que muestra correctamente el slogan:

```py
1. frase_alura[-1] + frase_alura[4] + frase_alura[5] + frase_alura[2] + frase_alura[3] + frase_alura[1]
2. frase_alura[-1] + frase_alura[3] + frase_alura[-2] + frase_alura[1] + frase_alura[2] + frase_alura[0]
3. frase_alura[-1] + frase_alura[-3] + frase_alura[-2] + frase_alura[-5] + frase_alura[-6] + frase_alura[-4]
4. frase_alura[5] + frase_alura[3] + frase_alura[4] + frase_alura[1] + frase_alura[2] + frase_alura[1]
```

Rta.

Opcion 2, de esta forma el eslogan se mostrará correctamente: Cursos de Tecnología y Negocios Digitales.

### **Descubriendo el NPS de varias notas de un curso**

¿Cuál de las opciones tiene la función con la sintaxis adecuada y con la invocación a la función correcta que genera el resultado de abajo?

```txt
El alumno es considerado Promotor
El alumno es considerado Neutro
El alumno es considerado Promotor
El alumno es considerado Detractor
El alumno es considerado Neutro
```

Rta.

```py
def clasificar_nps(notas):
  for nota in notas:
    if nota <= 6:
      print('El alumno es considerado Detractor')
    else:
      if nota <= 8:
        print('El alumno es considerado Neutro')
      else:
        print('El alumno es considerado Promotor')

clasificar_nps([9,8,10,6,7])
```

La sintaxis de esta función está correcta y la invocación a la función también está pasando los valores correctos que generan el resultado del enunciado.

### **¿Cuál es el valor de a?**

El condicional if es una estructura condicional que ejecuta una parte del código, en caso de que la afirmación sea verdadera. Si es falsa, ejecuta las afirmaciones dentro del else.

Sabiendo esto, observa el siguiente código:

```py
a=2

if a < 2:
  a = a + 1
else:
  a = a + 2
  if a < 4:
    a = a - 4
  else:
    a = a + 6
```

Analizando el código de arriba, podemos afirmar que:

Rta.

El valor de la variable a será 10.

### **Listas en Python**

Una lista se representa como una secuencia de objetos separados por coma y encerrados entre corchetes []. En Python, una lista puede almacenar elementos de diferentes tipos, como se muestra en la siguiente sintaxis:

```py
lista = ['int', False, True, '18', 2020]
```

Para descubrir el tipo de cada elemento, podemos crear un loop, como se muestra en el siguiente código:

```py
for elemento in lista:
  print(type(elemento))
```

Sabiendo esto, ¿Cuál será el resultado de la ejecución de los códigos de arriba?

Rta.

```py
<class 'str'>
<class 'bool'>
<class 'bool'>
<class 'str'>
<class 'int'>
```

El primer elemento de la lista es la palabra int de tipo string. El segundo y tercer elemento de la lista son de tipo booleano. El cuarto elemento es un string, pues está entre comillas, y el quinto elemento es un valor entero.

### **Para saber más: Nomenclatura de variables y tipo float**

Podemos pedirle a Python que recuerde un valor que queremos utilizar. Python almacenará este valor en una **variable**.

Variable es un nombre que hace referencia a un valor. Es como una etiqueta que colocamos a ese valor y cuando necesitamos usarla, la llamamos por el nombre que se le asignó en la etiqueta.

El comando de asignación (el signo igual =) crea una nueva variable y le asigna un valor, como vimos en clases.

#### **Float**

Además de los tipos que se vieron en la clase, existe un tipo para almacenar un valor decimal, como se muestra en el siguiente ejemplo:

```py
pi = 3.14
type(pi)
```

Al ejecutar ese código, podemos observar que la salida será:

[float](https://docs.python.org/pt-br/3/library/functions.html?highlight=float#float%28%29 "Python Doc")

Observa que, en vez de separar el valor con coma (3,14), usamos un punto (3.14).

#### **Nombre de las variables**

Programadores eligen nombres para variables que sean semánticas. Estos nombres pueden ser muy largos, pueden contener letras y números.

Es una convención entre los programadores de Python comenzar la variable con letras minúsculas y utilizar el underscore ( \_ ) para separar palabras como: mi_nombre, numero_de_registro, telefono_residencial. Este padrón se llama [snake case](https://en.wikipedia.org/wiki/Snake_case "Wikipedia").

---

## **El sistema de importación de librerías**

### **Importando Bibliotecas y Módulos**

Como vimos en aula, es posible importar bibliotecas y módulos en nuestra aplicación, posibilitando así el uso de códigos listos que nos ayudan a resolver diferentes problemas.

Con respecto a estas importaciones en Python, analiza las siguientes sentencias:

1. Colab no permite importar más de dos bibliotecas.
2. Importar bibliotecas, módulos y fragmentos de códigos, sólo funciona en Python. En ningún otro lenguaje existe esta idea de import.
3. Importamos un fragmento o módulo de una biblioteca para no sobrecargar la memoria de nuestra aplicación con código que no será usado.

Después de analizar las sentencias de arriba, selecciona la opción correcta.

Rta.

Solo la sentencia 3 es verdadera. Como vimos en el aula, importamos únicamente el módulo randrange de la biblioteca random, ya que solo necesitábamos generar números aleatorios con un rango establecido como parámetro.

### **Tamaño de una lista**

Vimos una función que nos permite saber el tamaño de una lista, la cantidad de elementos que contiene. Selecciona la opción correcta que nos devuelve el tamaño de la _lista_compras_:

Rta.

```py
len(lista_compras)
```

La función len() nos permite saber el tamaño de una lista. Solo no olvides pasar la variable entre los paréntesis.

### **Para saber más: Proyecto Open Source**

Python es un lenguaje de programación popular de código abierto (Open Source). Eso significa que Python se desarrolla bajo una licencia de código abierto aprobada por [OSI](https://es.wikipedia.org/wiki/Open_Source_Initiative "Wikipedia"), lo que lo hace de libre uso y distribución, incluso para uso comercial. La licencia de Python es administrada por [Python Software Foundation](https://es.wikipedia.org/wiki/Python_Software_Foundation "Wikipedia").

Para más información sobre [código abierto, haz clic en este link](https://es.wikipedia.org/wiki/Software_de_c%C3%B3digo_abierto "Wikipedia").

### **Haz lo que hicimos en aula: Importación de Bibliotecas y módulos**

Al importar la biblioteca random y el módulo randrange, estamos importando un método que podrá ser usado en nuestro programa. Podemos verificar su tipo conforme se muestra a continuación:

```py
from random import randrange
type(randrange)
```

Nuestra salida será: method, indicando que importamos el método para ser usado en nuestro programa.

Importa los métodos randrange y seed, crea 8 notas de 0 a 10, almacenando su contenido en una variable tipo lista llamada notas_matematica. Como los números generados se denominan pseudo aleatorios, utiliza el método seed escogiendo un valor de referencia.

---

## **Graficando con matplotlib**

### **Analizando gráficamente el desempeño escolar**

Para comparar el desempeño escolar de un determinado alumno, el colegio decidió crear un gráfico para cada materia, como se muestra en el siguiente código:

```py
import matplotlib.pyplot as plt

notas_matematica = ['Matemática',8,7,6,6,7,7,8,10]
notas_lenguaje = ['Lenguaje',9,9,9,8,5,6,8,5]
notas_geografia = ['Geografía',10,10,6,7,7,7,8,7]

notas = [notas_matematica, notas_lenguaje, notas_geografia]
```

¿Cuál de las siguientes alternativas generará los gráficos respectivos para cada materia?

Rta.

```py
for nota in notas:
 x = list(range(1, 9))
 y = nota[1:]
 plt.plot(x, y, marker='o')
 plt.xlabel('Exámenes')
 plt.ylabel('Notas')
 plt.title(nota[0])
 plt.show()
```

Observa que, la variable y almacenará la nota de cada materia a partir del índice 1, y, el título almacenado en el índice 0 será incluido en el gráfico.

### **Para saber más: Fue un excelente comienzo, pero esto continua...**

En este entrenamiento, todas las barreras fueron vencidas, y aprendiste los principales fundamentos de Python, desenvolviendo tu lógica de programación con énfasis en ciencia de datos.

Aprendiste, en la práctica, qué son variables y sus diferentes tipos, funciones, parámetros, condicionales, listas, imports de bibliotecas e incluso realizaste un gráfico usando Matplotlib.

Si todavía no has descargado tu Ebook de Python para Data Science, aprovecha y hazlo haciendo clic [aquí](https://caelum-online-public.s3.amazonaws.com/ESP-DS-24-alura-latam-apostilla-python-para-DS/ds-24-ebook-alura-latam-python-para-data-science-vbf.pdf "EBook")

Recuerda que, aquí en Alura, puedes continuar reforzando tus conocimientos en Python y Data Science. Tenemos los cursos que estudian las bibliotecas más populares de Python enfocadas en Data Science, como el curso de [Introducción a Data Science con Numpy](https://app.aluracursos.com/course/python-data-science-introduccion-lenguaje-numpy "Alura LATAM") o el curso de [Pandas Básico](https://app.aluracursos.com/course/python-data-science-funciones-librerias-pandas-basico "Alura LATAM").

No te olvides de calificar el curso, tu opinión es muy importante para que sigamos mejorando. Obtén tu certificado y compártelo, siéntete feliz por dar este primer gran paso.

**¡Muchas felicidades!**
