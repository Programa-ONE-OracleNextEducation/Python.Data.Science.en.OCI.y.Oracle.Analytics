# **Python Pandas - Tratamiento y análisis de datos**

## **Presentación del curso**

### **Preparando el ambiente**

En este curso utilizaremos **Google Colab** para escribir nuestro código Python y ejecutar los ejercicios. Para esto, necesitarás una cuenta de Google y acceder a [este enlace](https://colab.research.google.com/ "Google Colab").

Además, descarga [aquí](https://github.com/alura-es-cursos/1792-introducao-python-pandas/raw/1792-Aula1/1792-Aula1.zip "GitHub") el ZIP del proyecto inicial de nuestro entrenamiento, necesario para la continuidad del mismo.

### **El lenguaje Python**

Acerca del lenguaje Python, evalúe las siguientes afirmaciones:

1. Python es un lenguaje de programación de alto nivel, con soporte a múltiples paradigmas de programación
2. Python es un proyecto open source
3. Python es un lenguaje de programación interpretado

¿Cuáles afirmaciones son correctas?

Rta.

Todas las afirmaciones están correctas

### **Primer contacto con Pandas**

Tuvimos nuestro primer contacto con el paquete **Pandas**, de Python. Conocimos el _dataset_ de nuestro proyecto y trabajamos un poco con la estructura de datos principal de Pandas, el _DataFrame_. De las funcionalidades de Pandas que conocimos hasta ahora, indique cuál devuelve un resumen estadístico del conjunto de datos en el que estamos trabajando:

Rta.

.describe(). El método describe() genera un conjunto de estadísticas descriptivas de las columnas seleccionadas de un DataFrame.

---

## **Importando datos**

### **Conociendo Colab**

Hablando sobre nuestra herramienta de trabajo en el curso, Colab, revise las siguientes afirmaciones:

1. Para acceder a la documentación de una biblioteca desde Colab, usamos la tecla TAB.
2. Para modificar una celda del notebook para tipo Code, basta presionar las teclas ESC + C.
3. Una de las formas de ejecutar el código de una celda en el notebook es presionando las teclas SHIFT + ENTER.

¿Cuáles afirmaciones son correctas?

Rta.

Las afirmaciones 1 y 3 son correctas. Para obtener más información sobre las teclas de método abreviado de Colab, vaya a la barra de menú y haga clic en Herramientas -> Atajos del teclado, o presione las teclas Ctrl + M H.

Recuerde colocar el cursor del mouse sobre el elemento del cual le gustaría ver la documentación antes de presionar la tecla TAB.

### **Informaciones de un Dataframe**

Considere el siguiente código Python en Colab:

```py
import pandas as pd
data = [['Fulano', 12, 7.0, True],
        ['Sicrano', 15, 3.5, False],
        ['Beltrano', 18, 9.3, True]]
datos = pd.DataFrame(data,
        columns = ['Alumno', 'Edad', 'Nota', 'Aprobado'])
datos
```

Al ejecutar este código, obtendremos el DataFrame siguiente:

|     | Alumno   | Edad | Nota | Aprobado |
| --- | -------- | ---- | ---- | -------- |
| 0   | Fulano   | 12   | 7.0  | True     |
| 1   | Sicrano  | 15   | 3.5  | False    |
| 2   | Beltrano | 18   | 9.3  | True     |

Para obtener una tabla que contenga los nombres de las variables y sus respectivos tipos de datos, igual al siguiente ejemplo, ¿qué líneas de código debemos ejecutar en Colab?

| Variables | Tipos de Datos |
| --------- | -------------- |
| Alumno    | object         |
| Edad      | int64          |
| Nota      | float64        |
| Aprobado  | bool           |

Rta.

```py
tipos_de_datos = pd.DataFrame(datos.dtypes,
    columns = ['Tipos de Datos'])
tipos_de_datos.columns.name = 'Variables'
tipos_de_datos
```

Aprender a manipular un DataFrame es esencial en un trabajo de análisis de datos.

### **Funciones para obtención de datos**

Pandas ofrece un conjunto muy amplio de funciones que posibilitan la obtención de datos a partir de diferentes formatos de archivos. Estas funciones importan datos tabulares a partir de archivos en diferentes formatos (csv, xlsx, txt, etc.) para dentro de estructuras de datos de Pandas (Series y DataFrames). En base a esto, evalúe las siguientes afirmaciones y seleccione las correctas:

Rta.

- Cuando usamos la función read_html() en una URL o archivo HTML que contenga más de una tabla, cada una es convertida en un DataFrame y todas se almacenan en una lista de Python, es decir, la función read_html() devuelve una lista de DataFrames. Para obtener más información sobre la función read_html(), acceda a la documentación haciendo clic [aquí](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_html.html?highlight=read_html#pandas.read_html "Documentation").
- Cuando se usa la función read_html(), es posible pasar como parámetro de entrada (fuente de los datos) una URL. Es posible pasar una URL como fuente de datos y también un archivo en formato HTML.

---

## **Series y Index**

### **Utilizando métodos en Pandas**

Pandas proporciona un método de DataFrame para eliminar líneas duplicadas. Este método es el drop_duplicates(), que presenta un conjunto de argumentos de configuración. Considerando las dos líneas de código a continuación:

```py
import pandas as pd
datos = pd.DataFrame([1, 1, 2, 3, 3, 3, 4, 4], columns = ['X'])
```

¡Marque la opción donde el método drop_duplicates() se aplica correctamente!

Rta.

```py
datos.drop_duplicates(inplace = True)
```

Configurando el parámetro inplace como True, la modificación sustituye los propios datos, sin la necesidad de crear un DataFrame para almacenar el resultado.

### **Índice de las líneas de un Dataframe**

No siempre está indicado, pero hay varias formas de modificar el _index_ de una estructura de datos en pandas. Suponiendo que **df** es cualquier _DataFrame_, indique la opción que muestra una forma correcta de modificar el _index_ de este _DataFrame_.

Rta.

```py
df.index = range(df.shape[0])
```

También puede usar el método _len()_ de Python para devolver el número de registros de un _DataFrame_. El código se vería así:

```py
df.index = range(len(df))
```

### **Formas de creación de un Dataframe**

¡Evalúe los elementos siguientes y seleccione las salidas (Out) correctas!

Nota: Los códigos siguientes son complejos, por eso puedes/debes usar Colab para probar cada uno.

Rta.

**In [1]:**

```py
datos = {'A': {'X': 1, 'Y': 3}, 'B': {'X': 2, 'Y': 4}}
df = pd.DataFrame(datos)
df
```

**Out [1]:**

|     | A   | B   |
| --- | --- | --- |
| X   | 1   | 2   |
| Y   | 3   | 4   |

Es posible crear DataFrames a partir de diccionarios Python.

**In [3]:**

```py
datos = [[1, 2, 3], [4, 5, 6]]
index = 'X,Y'.split(',')
columns = list('CBA')[::-1]
df = pd.DataFrame(datos, index, columns)
df
```

**Out [3]:**

|     | A   | B   | C   |
| --- | --- | --- | --- |
| X   | 1   | 2   | 3   |
| Y   | 4   | 5   | 6   |

Python es muy versátil cuando se trata de crear listas. Observe que los argumentos index y columns reciben siempre una lista.

### **Conociendo mejor las estructuras de datos**

Evalúe las siguientes afirmaciones:

1. Un DataFrame es una estructura de datos tabular formada por un conjunto de Series.
2. Es posible crear una Series a partir de dos columnas de un DataFrame.
3. No es posible realizar operaciones aritméticas entre estructuras de datos de pandas.

Rta.

Sólo la 1 está correcta. Un DataFrame es un conjunto de Series.

### **Lo que aprendimos**

- Cómo seleccionar una variable del Dataframe (por ejemplo, datos['Tipo'] o datos.Tipo)
- Que un Dataframe está compuesto de varios Series
- Cómo eliminar duplicados (usando el método drop_duplicates())
- Cómo redefinir el index de un Dataframe y Series (atributo index)
- Cómo concatenar Dataframes (recordando el axis)
- Cómo crear nuevos Dataframes basados ​​en estructuras de datos Python (lista, diccionarios o tuplas)

---

## **Filtrando datos**

### **Determinando selecciones**

Vea el código que crea un DataFrame a partir de letras y números.

```py
import pandas as pd

numeros = [i for i in range(11)]
letras = [chr(i + 65) for i in range(11)]
nome_coluna = ['N']

df = pd.DataFrame(data = numeros, index = letras, columns = nome_coluna)
```

Basándonos en este Dataframe, generamos una selección para filtrar los datos:

```py
seleccion= df['N'].isin([i for i in range(11) if i % 2 == 0])
df = df[seleccion]
df
```

Nota: ¡No dude en probar el código en su Colab!

Seleccione la opción que muestra el resultado de ejecutar el código anterior:

Rta.

| N   |     |
| --- | --- |
| A   | 0   |
| C   | 2   |
| E   | 4   |
| G   | 6   |
| I   | 8   |
| K   | 10  |

El código anterior crea un _DataFrame_ con una columna llamada **N** y le asigna los números del 0 al 10. Con el método _isin()_, seleccionamos sólo los números pares de **N**.

### **Revisando lo que aprendimos**

Juzgue lo siguiente:

1. La función export_csv() crea un archivo CSV a partir de un DataFrame de pandas.
2. El método isin() devuelve una serie booleana a partir de un DataFrame.
3. La función read_csv() lee un archivo CSV y crea una Series.

Rta.

Sólo la 2 está correcta. DataFrames o Series booleanas se utilizan bastante en procesos de selecciones.

### **El funcionamiento de sort_index**

Considere el siguiente DataFrame llamado como df:

|     | C   | B   | A   |
| --- | --- | --- | --- |
| Z   | 9   | 6   | 3   |
| Y   | 8   | 5   | 2   |
| X   | 7   | 4   | 1   |

Después de ejecutar el siguiente conjunto de rutinas en Colab:

```py
df.sort_index()
df.sort_index(axis = 1)
df
```

¿Cuál es el resultado obtenido?

Rta.

|     | C   | B   | A   |
| --- | --- | --- | --- |
| Z   | 9   | 6   | 3   |
| Y   | 8   | 5   | 2   |
| X   | 7   | 4   | 1   |

Para que el método sort_index() modifique el DataFrame, el argumento inplace debe cambiarse para True.

### **Más sobre clasificaciones**

Observe los siguientes esquemas:

**df_A:**

|     | Nombre | Sexo | Edad |
| --- | ------ | ---- | ---- |
| 0   | Ary    | M    | 21   |
| 1   | Katia  | F    | 19   |
| 2   | Carlos | M    | 50   |
| 3   | Beto   | M    | 29   |
| 4   | Bruna  | F    | 31   |
| 5   | Ana    | F    | 42   |

**df_B:**

|     | Nombre | Sexo | Edad |
| --- | ------ | ---- | ---- |
| 5   | Ana    | F    | 42   |
| 4   | Bruna  | F    | 31   |
| 1   | Katia  | F    | 19   |
| 0   | Ary    | M    | 21   |
| 3   | Beto   | M    | 29   |
| 2   | Carlos | M    | 50   |

¿Cuál de las siguientes líneas de código convierte df_A en df_B?

Rta.

```py
df_B = df_A.sort_values(by = ['Sexo', 'Nombre'])
```

Consulte la documentación para obtener más información sobre el método sort_values().

### **Lo que aprendimos**

- Crear una _Series_ booleana utilizando el método **isin(..)** a partir de un _dataframe_
- Filtrar los datos de un _dataframe_ utilizando una _Series_ booleana
- Exportar y guardar los datos de un _dataframe_(método **to_csv()**)
- Ordenar los datos de un dataframe (métodos **sort_values​​()** y **sort_index()**)

---

## **Frecuencias de inmuebles**

### **Dataframe para los próximos ejercicios**

Considere el siguiente DataFrame para responder a los próximos ejercicios:

```py
import pandas as pd
alumnos = pd.DataFrame({
'Nombre': ['Ary', 'Katia', 'Denis', 'Beto', 'Bruna', 'Dara', 'Carlos', 'Alice'],
'Sexo': ['M', 'F', 'M', 'M', 'F', 'F', 'M', 'F'],
'Edad': [15, 27, 56, 32, 42, 21, 19, 35],
'Notas': [7.5, 2.5, 5.0, 10, 8.2, 7, 6, 5.6],
'Aprobado': [True, False, False, True, True, True, False, False]},
columns = ['Nombre', 'Edad', 'Sexo', 'Notas', 'Aprobado'])
```

|     | Nombre | Edad | Sexo | Notas | Aprobado |
| --- | ------ | ---- | ---- | ----- | -------- |
| 0   | Ary    | 15   | M    | 7.5   | True     |
| 1   | Katia  | 27   | F    | 2.5   | False    |
| 2   | Denis  | 56   | M    | 5.0   | False    |
| 3   | Beto   | 32   | M    | 10.0  | True     |
| 4   | Bruna  | 42   | F    | 8.2   | True     |
| 5   | Dara   | 21   | F    | 7.0   | True     |
| 6   | Carlos | 19   | M    | 6.0   | False    |
| 7   | Alice  | 35   | F    | 5.6   | False    |

Utilizando las técnicas de selección presentadas en esta aula, realice los conjuntos de selecciones propuestos en los siguientes ejercicios.

**Selección (nivel 1)**

Cree un DataFrame sólo con los estudiantes aprobados.

Rta.

```py
seleccion = alumnos['Aprobado'] == True
aprobados = alumnos[seleccion]
aprobados
```

**Selección (nivel 2)**

Cree un DataFrame sólo con las estudiantes aprobadas.

Rta

```py
seleccion = (alumnos.Aprobado == True) & (alumnos.Sexo == 'F')
aprobadas = alumnos[seleccion]
aprobadas
```

**Selección (nivel 3)**

Cree sólo una visualización de los alumnos con edades entre 10 a 20 años o con edad mayor/igual a 40 años.

Rta.

```py
seleccion = (alumnos.Edad > 10) & (alumnos.Edad < 20) | (alumnos.Edad >= 40)
alumnos[seleccion]
```

**Selección (nivel 4)**

Cree un DataFrame sólo con los estudiantes reprobados y mantenga en este DataFrame sólo las columnas Nombre, Sexo y Edad, en este orden.

Rta.

```py
seleccion = alumnos['Aprobado'] == False
reprobados = alumnos[['Nombre', 'Sexo', 'Edad']][seleccion]
reprobados
```

```py
seleccion = alumnos['Aprobado'] == False
reprobados = alumnos.loc[seleccion, ['Nombre', 'Sexo', 'Edad']]
reprobados
```

**Selección (nivel 5)**

Crea una visualización con los tres alumnos más jóvenes.

```py
alumnos.sort_values(by = ['Edad'], inplace = True)
alumnos.iloc[:3]
```

### **Lo que aprendimos**

- Métodos de selección y frecuencias
  - Selección con la condición **OR** (|)
  - Selección con la condición **AND** (&)
- Cómo crear un Index con split()
- Selección por línea y columna en un dataframe:
  - Usando índices numéricos y rótulos de las líneas

---

## **Tratando los datos faltantes**

Conocimientos básicos sobre missing values
Próxima Actividad

Seleccione la opción que presenta una declaración falsa:

Rta.

- El uso del método info() es la forma de identificar la presencia de información nula en un DataFrame.

- El método notnull() devuelve lo opuesto al método isnull().

- La aplicación del método isnull() en un DataFrame devuelve un DataFrame booleano, del mismo tamaño que el original, indicando si los valores son nulos.

- **Falso**. El método dropna() es la única forma de eliminar información nula de un DataFrame. El método dropna() no es la única forma de eliminar los valores nulos de un DataFrame. En nuestras aulas, les mostraremos más de una forma de hacer esto.

### **Revisando el proceso**

Utilizando los recursos aprendidos en esta aula, juzgue el conjunto de operaciones de limpieza del DataFrame a continuación:

```py
inmuebles= pd.DataFrame([['Departamento', None, 970, 68],
                        ['Departamento', 2000, 878, 112],
                        ['Casa', 5000, None, 500],
                        ['Departamento', None, 1010, 170],
                        ['Departamento', 1500, 850, None],
                        ['Casa', None, None, None],
                        ['Departamento', 2000, 878, None],
                        ['Departamento', 1550, None, 228],
                        ['Departamento', 2500, 880, 195]],
                        columns = ['Tipo', 'Valor', 'Mantenimiento', 'Impuesto'])
```

|     | Tipo         | Valor  | Mantenimiento | Impuesto |
| --- | ------------ | ------ | ------------- | -------- |
| 0   | Departamento | NaN    | 970.0         | 68.0     |
| 1   | Departamento | 2000.0 | 878.0         | 112.0    |
| 2   | Casa         | 5000.0 | NaN           | 500.0    |
| 3   | Departamento | NaN    | 1010.0        | 170.0    |
| 4   | Departamento | 1500.0 | 850.0         | NaN      |
| 5   | Casa         | NaN    | NaN           | NaN      |
| 6   | Departamento | 2000.0 | 878.0         | NaN      |
| 7   | Departamento | 1550.0 | NaN           | 228.0    |
| 8   | Departamento | 2500.0 | 880.0         | 195.0    |

1. Elimina los registros que tengan la columna Valor nula:

```py
inmuebles.dropna(subset = ['Valor'], inplace = True)
inmuebles
```

|     | Tipo         | Valor  | Mantenimiento | Impuesto |
| --- | ------------ | ------ | ------------- | -------- |
| 1   | Departamento | 2000.0 | 878.0         | 112.0    |
| 2   | Casa         | 5000.0 | NaN           | 500.0    |
| 4   | Departamento | 1500.0 | 850.0         | NaN      |
| 6   | Departamento | 2000.0 | 878.0         | NaN      |
| 7   | Departamento | 1550.0 | NaN           | 228.0    |
| 8   | Departamento | 2500.0 | 880.0         | 195.0    |

2. Elimina los inmuebles de tipo Departamento que tengan la columna Mantenimiento nula:

```py
seleccion = (inmuebles['Tipo'] == 'Departamento') & (inmuebles['Condominio'].isnull())
inmuebles= inmuebles[seleccion]
inmuebles
```

|     | Tipo         | Valor  | Mantenimiento | Impuesto |
| --- | ------------ | ------ | ------------- | -------- |
| 1   | Departamento | 2000.0 | 878.0         | 112.0    |
| 2   | Casa         | 5000.0 | NaN           | 500.0    |
| 4   | Departamento | 1500.0 | 850.0         | NaN      |
| 6   | Departamento | 2000.0 | 878.0         | NaN      |
| 8   | Departamento | 2500.0 | 880.0         | 195.0    |

3. Reemplaza los valores nulos que quedan en las variables Mantenimiento y Impuesto por cero:

```py
inmuebles = inmuebles.fillna({'Mantenimiento': 0, 'Impuesto': 0})
inmuebles
```

|     | Tipo         | Valor  | Mantenimiento | Impuesto |
| --- | ------------ | ------ | ------------- | -------- |
| 1   | Departamento | 2000.0 | 878.0         | 112.0    |
| 2   | Casa         | 5000.0 | 0.0           | 500.0    |
| 4   | Departamento | 1500.0 | 850.0         | 0.0      |
| 6   | Departamento | 2000.0 | 878.0         | 0.0      |
| 8   | Departamento | 2500.0 | 880.0         | 195.0    |

4. Reconstruye el índice del DataFrame resultante:

```py
inmuebles.index = range(inmuebles.shape[0])
inmuebles
```

|     | Tipo         | Valor  | Mantenimiento | Impuesto |
| --- | ------------ | ------ | ------------- | -------- |
| 0   | Departamento | 2000.0 | 878.0         | 112.0    |
| 1   | Casa         | 5000.0 | 0.0           | 500.0    |
| 2   | Departamento | 1500.0 | 850.0         | 0.0      |
| 3   | Departamento | 2000.0 | 878.0         | 0.0      |
| 4   | Departamento | 2500.0 | 880.0         | 195.0    |

Rta.

Las alternativas 1, 3 y 4 son correctas. La alternativa 2 es incorrecta. Lo que queremos en este caso es eliminar los departamentos que tienen Mantenimiento nulo. Para eso, necesitaríamos modificar la segunda línea del código anterior:

```py
inmuebles = inmuebles[~seleccion]
```

### **Llenando datos faltantes**

Suponga el siguiente conjunto de datos:

```py
atletas = pd.DataFrame([
['Marcos', 9.62], ['Pedro', None], ['Juan', 9.69],
['Beto', 9.72], ['Sandro', None], ['Denis', 9.69],
['Ary', None], ['Carlos', 9.74]],
columns = ['Corredor', 'Mejor tiempo'])
atletas
```

|     | Corredor | Mejor tiempo |
| --- | -------- | ------------ |
| 0   | Marcos   | 9.62         |
| 1   | Pedro    | NaN          |
| 2   | Juan     | 9.69         |
| 3   | Beto     | 9.72         |
| 4   | Sandro   | NaN          |
| 5   | Denis    | 9.69         |
| 6   | Ary      | NaN          |
| 7   | Carlos   | 9.74         |

Tenga en cuenta que el tiempo de algunos atletas no se registró debido a un error en el proceso de medición. Observando los datos y teniendo un conocimiento previo del desempeño de cada atleta, usted, como científico de datos, decide que es razonable para este caso específico atribuir el tiempo promedio de todos los atletas a los datos faltantes. Marque la forma correcta de hacer esto en Colab:

Rta.

```py
atletas.fillna(atletas.mean(), inplace = True)
```

Por lo tanto, el promedio de los valores no-nulos se asignará a los registros con missing values.

### **Lo que aprendimos**

- Tratamiento de datos faltantes
- Cómo identificar valores nulos (missing values)
  El método **isnull()** indica si los valores son nulos
  El método **notnull()** devuelve lo opuesto al método **isnull()**
  El método **info()** también es una forma de verificar la presencia de valores nulos
- Cómo eliminar valores nulos con el método **dropna()**
- Tratamiento condicional
- Inversión de valores booleanos con **~**
- Cómo reemplazar los missing values con el método **fillna()**
- Métodos de interpolación: **ffill**, **bfill** y **mean()**

---

## **Nuevas variables**

### **Identifique el error al crear variables**

Hablando de crear nuevas variables para un DataFrame, analice las opciones a continuación e indique la que presente un error durante la ejecución. Considere el DataFrame a continuación:

```py
import pandas as pd
alumnos = pd.DataFrame({
'Nombre': ['Ary', 'Katia', 'Denis', 'Beto', 'Bruna', 'Dara', 'Carlos', 'Alice'],
'Sexo': ['M', 'F', 'M', 'M', 'F', 'F', 'M', 'F'],
'Edad': [15, 27, 56, 32, 42, 21, 19, 35],
'Notas': [7.5, 2.5, 5.0, 10, 8.2, 7, 6, 5.6]},
columns = ['Nombre', 'Edad', 'Sexo', 'Notas'])
```

|     | Nombre | Edad | Sexo | Notas |
| --- | ------ | ---- | ---- | ----- |
| 0   | Ary    | 15   | M    | 7.5   |
| 1   | Katia  | 27   | F    | 2.5   |
| 2   | Denis  | 56   | M    | 5.0   |
| 3   | Beto   | 32   | M    | 10.0  |
| 4   | Bruna  | 42   | F    | 8.2   |
| 5   | Dara   | 21   | F    | 7.0   |
| 6   | Carlos | 19   | M    | 6.0   |
| 7   | Alice  | 35   | F    | 5.6   |

Rta.

```py
alumnos['Notas-Média(Notas)'] = alumnos['Notas']
    .apply(lambda x: x - alumnos['Notas'].mean())
```

```py
alumnos['Rango Edad'] = alumnos['Edad']
    .apply(lambda x: 'Menor que 20 años' if x < 20
        else ('Entre 20 y 40 años' if (x >= 20 and x <= 40)
            else 'Mayor que 40 años'))
```

```py
alumnos['Notas-Média(Notas)'] =
    alumnos.Notas - alumnos.Notas.mean()
```

**No ejecuta bien**

```py
alumnos['Rango Edad'] = alumnos['Edad']
    .apply(lambda x: 'Menor que 20 años' if x < 20
        elif ('Entre 20 y 40 años' if (x >= 20 and x <= 40)
            else 'Mayor que 40 años'))
```

El único error en esta línea de código está en la declaración elif, cambiándolo por else el código se ejecuta perfectamente.

### **Métodos de exclusión de variables**

Considere un DataFrame (df) con cuatro variables, rotuladas como A, B, C y D. ¿Cuál de las siguientes opciones presenta una forma correcta de excluir variables de un DataFrame?

Rta.

- **df.pop('B')**. El método pop() excluye sólo una columna a la vez.
- **del df['D']**. El método de excluir columnas usando del tiene algunas restricciones. No es posible eliminar dos o más columnas a la vez y no podemos usar la siguiente notación: del df.D
- **df.drop(['C', 'D'], axis = 1, inplace = True)**. Usando drop(), podemos excluir no sólo columnas (axis = 1), sino también líneas (default).

### **Probando monedas**

Supongamos que estamos probando las monedas que utilizarán los árbitros de fútbol en las competiciones de la Copa del Mundo. Actualmente, estas monedas están personalizadas y se utilizan en determinados momentos del juego para decidir, por suerte, una disputa. Nuestro objetivo es verificar que las monedas que se utilizarán en los juegos no sean viciadas.

Para ello se realizaron pruebas con cinco monedas y los resultados fueron los siguientes:

```py
m1 = 'CCcCCccCCCccCcCccCcCcCCCcCCcccCCcCcCcCcccCCcCcccCc'
m2 = 'CCCCCccCccCcCCCCccCccccCccCccCCcCccCcCcCCcCccCccCc'
m3 = 'CccCCccCcCCCCCCCCCCcccCccCCCCCCccCCCcccCCCcCCcccCC'
m4 = 'cCCccCCccCCccCCccccCcCcCcCcCcCcCCCCccccCCCcCCcCCCC'
m5 = 'CCCcCcCcCcCCCcCCcCcCCccCcCCcccCccCCcCcCcCcCcccccCc'
```

Arriba, tenemos el resultado de 50 lanzamientos de cada moneda (m1, m2, m3, m4 y m5), donde c representa la ocurrencia del evento CARA y C representa la ocurrencia del evento CROWN (sello o corona).

Para sacar nuestras conclusiones, necesitamos construir el siguiente DataFrame:

|     | Faces | m1      | m2      | m3      | m4      | m5      |
| --- | ----- | ------- | ------- | ------- | ------- | ------- |
| C   | Crown | Frec. C | Frec. C | Frec. C | Frec. C | Frec. C |
| c   | Cara  | Frec. c | Frec. c | Frec. c | Frec. c | Frec. c |

Donde Frec. C y Frec. c son, respectivamente, las frecuencias de CROWN y CARA de cada prueba.

Marque la opción que muestre el código necesario para realizar esta tarea.

Rta.

```py
eventos = {'m1': list(m1),
                'm2': list(m2),
                'm3': list(m3),
                'm4': list(m4),
                'm5': list(m5)}
monedas = pd.DataFrame(eventos)
df = pd.DataFrame(data = ['Cara', 'Crown'],
                    index = ['c', 'C'],
                    columns = ['Faces'])
for item in monedas:
    df = pd.concat([df, monedas[item].value_counts()],
                    axis = 1)
df
```

Observe que value_counts() es una funcionalidad aplicable sólo a Series, y por eso, necesitamos ejecutar el código para cada columna del DataFrame, individualmente.

### **Lo que aprendimos**

- Como crear variables
- Cómo excluir variables usando **del**, **pop()** y **drop()**
- Contadores, a través de **value_counts()**

---

## **Estadísticas descriptivas**

### **Sumando informaciones**

Considere el siguiente DataFrame para responder al próximo ejercicio:

```py
import pandas as pd
alumnos = pd.DataFrame({
'Nombre': ['Ary', 'Katia', 'Denis', 'Beto', 'Bruna', 'Dara', 'Carlos', 'Alice'],
'Sexo': ['M', 'F', 'M', 'M', 'F', 'F', 'M', 'F'],
'Edad': [15, 27, 56, 32, 42, 21, 19, 35],
'Notas': [7.5, 2.5, 5.0, 10, 8.2, 7, 6, 5.6],
'Aprobado': [True, False, False, True, True, True, False, False]},
columns = ['Nombre', 'Edad', 'Sexo', 'Notas', 'Aprobado'])
```

|     | Nombre | Edad | Sexo | Notas | Aprobado |
| --- | ------ | ---- | ---- | ----- | -------- |
| 0   | Ary    | 15   | M    | 7.5   | True     |
| 1   | Katia  | 27   | F    | 2.5   | False    |
| 2   | Denis  | 56   | M    | 5.0   | False    |
| 3   | Beto   | 32   | M    | 10.0  | True     |
| 4   | Bruna  | 42   | F    | 8.2   | True     |
| 5   | Dara   | 21   | F    | 7.0   | True     |
| 6   | Carlos | 19   | M    | 6.0   | False    |
| 7   | Alice  | 35   | F    | 5.6   | False    |

¿Cómo debemos proceder para obtener un DataFrame con las notas promedio de los alumnos, con dos casas decimales, agrupado por su sexo?

Rta.

```py
sexo = alumnos.groupby('Sexo')
sexo = pd.DataFrame(sexo['Notas'].mean().round(2))
sexo.columns = ['Notas Médias']
sexo
```

Aprenderemos más sobre grupos en las próximas aulas.

### **Seleccionando las estadísticas descriptivas**

La creación de agrupamientos con el método _groupby()_ hace que sea mucho más fácil sumar las informaciones en un DataFrame. El método _describe()_ aplicado a un agrupamiento genera un conjunto de estadísticas descriptivas que es muy útil en el proceso de análisis de datos, como se muestra en el siguiente ejemplo:

```py
precios = pd.DataFrame([
['Feria', 'Cebolla', 2.5],
         ['Mercado', 'Cebolla', 1.99],
         ['Supermercado', 'Cebolla', 1.69],
         ['Feria', 'Tomate', 4],
         ['Mercado', 'Tomate', 3.29],
         ['Supermercado', 'Tomate', 2.99],
         ['Feria', 'Papa', 4.2],
         ['Mercado', 'Papa', 3.99],
         ['Supermercado', 'Papa', 3.69]],
columns = ['Local', 'Producto', 'Precio'])
precios
```

|     | Local        | Producto | Precio |
| --- | ------------ | -------- | ------ |
| 0   | Feria        | Cebolla  | 2.5    |
| 1   | Mercado      | Cebolla  | 1.99   |
| 2   | Supermercado | Cebolla  | 1.69   |
| 3   | Feria        | Tomate   | 4      |
| 4   | Mercado      | Tomate   | 3.29   |
| 5   | Supermercado | Tomate   | 2.99   |
| 6   | Feria        | Papa     | 4.2    |
| 7   | Mercado      | Papa     | 3.99   |
| 8   | Supermercado | Papa     | 3.69   |

```py
productos = precios.groupby('Producto')
productos.describe().round(2)
```

| Producto | Precio | mean | std  | min  | 25%  | 50%  | 75%  | max |
| -------- | ------ | ---- | ---- | ---- | ---- | ---- | ---- | --- |
| Papa     | 3.0    | 3.96 | 0.26 | 3.69 | 3.84 | 3.99 | 4.10 | 4.2 |
| Cebolla  | 3.0    | 2.06 | 0.41 | 1.69 | 1.84 | 1.99 | 2.24 | 2.5 |
| Tomate   | 3.0    | 3.43 | 0.52 | 2.99 | 3.14 | 3.29 | 3.64 | 4.0 |

Usando el DataFrame anterior (precios), qué código se necesitaría para generar la siguiente visualización:

| Producto | Promedio | Desviación Estándar | Mínimo | Máximo |
| -------- | -------- | ------------------- | ------ | ------ |
| Papa     | 3.96     | 0.26                | 3.69   | 4.2    |
| Cebolla  | 2.06     | 0.41                | 1.69   | 2.5    |
| Tomate   | 3.43     | 0.52                | 2.99   | 4.0    |

Rta.

```py
estadisticas = ['mean', 'std', 'min', 'max']
nombres = {'mean': 'Promedio', 'std': 'Desviación Estándar',
    'min': 'Mínimo', 'max': 'Máximo'}
productos['Precio'].aggregate(estadisticas)
    .rename(columns = nombres).round(2)
```

El método _aggregate()_ permite seleccionar un conjunto personalizado de estadísticas. Es posible declarar el método de forma simplificada, usando _agg()_, como se muestra a continuación:

```py
producto['Precio'].agg(['mean', 'std'])
```

### **Conociendo la función cut()**

Revise las afirmaciones a continuación:

1. La función cut() es una herramienta de pandas que ayuda a crear distribuciones de frecuencia.
2. Es posible crear labels para las clases creadas por la función cut().
3. La función cut() permite especificar los límites de cada clase.

Rta.

Todas son verdaderas.

### **Lo que aprendimos**

- Cómo crear agrupaciones con **groupby()**
- Estadísticas descriptivas con **describe()** y **aggregate()**
- Cómo cambiar el nombre de las columnas con **rename()**
- Cómo hacer gráficos con el paquete **Matplotlib**
- Cómo crear rangos de valor con **cut()**

---

## **Excluyendo Outliers**

### **Estadísticas del box plot**

Considere la siguiente imagen:

![Box Plot]( "Box Plot")

![Box Plot](./img/compressed_box-plot.png "Box Plot")

Nuestro objetivo en este ejercicio es obtener el conjunto de estadísticas representadas en la figura anterior. Para ello, descargue aquí el archivo alquiler_muestra.csv y utilícelo para realizar sus análisis utilizando como variable objetivo el Valor m2 (valor del metro cuadrado). Recordando que Q1 representa el primer cuartil y Q3 el tercer cuartil, seleccione el elemento con la respuesta correcta (considere sólo dos casas decimales):

Rta.

```py
[Q1] → 21.25
[Q3] → 42.31
[IIQ] → 21.06
[Q1 - 1.5 * IIQ] → -10.34
[Q3 + 1.5 * IIQ] → 73.90
```

### **Sobre el box plot**

Evalúe las siguientes afirmaciones:

1. El box plot es la única forma de identificar y excluir outliers de una base de datos.
2. El intervalo intercuartil es la diferencia entre el primer y el tercer cuartil. (IIQ = Q1 - Q3)
3. Sólo con el primer y el tercer cuartil, es posible construir todos los parámetros necesarios para definir un box plot.

Rta.

Todas las alternativas son incorrectas. Existen varias técnicas para identificar y excluir outliers. Además, el intervalo intercuartil es la diferencia entre el tercer y el primer cuartil, y además de Q1 y Q3, se necesitan otros valores (como Q2 / Mediana) para definir un box plot.

### **La biblioteca matplotlib**

Probemos nuestro conocimiento sobre el paquete matplotlib. Para este ejercicio, considere el archivo alquiler_muestra.csv e indique en las opciones a continuación qué código es necesario para generar los gráficos de la siguiente figura:

![matplotlib-pie](./img/matplotlib-pie.png "matplotlib-pie")

En este ejercicio, estamos presentando el gráfico de pizza que puede ser obtenido aplicando el método pie(), de matplotlib. Considere el código inicial a continuación para elegir la alternativa correcta:

```py
%matplotlib inline
import pandas as pd
import matplotlib.pyplot as plt
plt.rc('figure', figsize = (15, 7))

datos = pd.read_csv('alquiler_muestra.csv', sep = ';')
```

Rta.

```py
area = plt.figure()
g1 = area.add_subplot(1, 2, 1)
g2 = area.add_subplot(1, 2, 2)
grupo1 = datos.groupby('Tipo Agrupado')['Valor']
label = grupo1.count().index
valores = grupo1.count().values
g1.pie(valores, labels = label, autopct='%1.1f%%')
g1.set_title('Total de Inmuebles por Tipo Agrupado')
grupo2 = datos.groupby('Tipo')['Valor']
label = grupo2.count().index
valores = grupo2.count().values
g2.pie(valores, labels = label, autopct='%1.1f%%', explode = (.1, .1, .1, .1, .1))
g2.set_title('Total de Inmuebles por Tipo')
```

### **Lo que aprendimos**

- Cómo identificar y eliminar **outliers** con **box plot**
- Cómo hacer un gráfico de pizza con la aplicación del método **pie()**, de la biblioteca **matplotlib**
