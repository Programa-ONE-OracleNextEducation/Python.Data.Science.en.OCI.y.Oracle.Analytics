# **Python para Data Science - Funciones, Librerías y Pandas básico**

## **Presentación del curso**

### **Preparando el ambiente**

En este curso utilizaremos **Google Colab** para escribir nuestro código Python y ejecutar los ejercicios. Para esto, necesitarás una cuenta de Google y acceder a [este enlace](https://colab.research.google.com/ "Google Colab").

Además, descarga [aquí]() el ZIP del proyecto inicial de nuestro entrenamiento, necesario para la continuidad del mismo.

El lenguaje Python

Acerca del lenguaje Python, evalúe las siguientes afirmaciones:

1. Python es un lenguaje de programación de alto nivel, con soporte a múltiples paradigmas de programación
2. Python es un proyecto open source
3. Python es un lenguaje de programación interpretado

¿Qué afirmaciones son correctas?

Rta.

Todas las afirmaciones son correctas. En nuestro notebook tenemos una breve definición del lenguaje Python.

### **Primer contacto con Pandas**

Tuvimos nuestro primer contacto con el paquete Pandas, de Python. Conocimos el dataset de nuestro proyecto y trabajamos un poco con la estructura de datos principal de Pandas, el DataFrame. De las funcionalidades de Pandas que conocimos hasta ahora, indica cuál devuelve un resumen estadístico del conjunto de datos en el que estamos trabajando:

Rta.

```py
.describe()
```

El método describe() genera un conjunto de estadísticas descriptivas de las columnas seleccionadas de un DataFrame.

---

## **Listas estáticas: Tuplas**

### **Formas de crear una tupla**

Las tuplas son secuencias inmutables que se utilizan para almacenar colecciones de elementos.

Entre las alternativas a continuación, ¿cuáles muestran formas de crear una tupla?

Rta.

```py
tuple( [ 1, 2, 3 ] )
```

- El tipo tuple permite crear tuplas, utilizando algún tipo de iterador como argumento. En este caso, fue utilizada una lista de Python.

```py
1, 2, 3
```

- No es necesario usar paréntesis para definir una tupla, basta con definir los elementos y separarlos con comas.
  Alternativa correta

```py
( 1, 2, 3 )
```

- Podemos crear una tupla usando paréntesis, con sus elementos separados por comas.

### **Seleccionando elementos en tuplas**

En nuestro primer entrenamiento de Python para Data Science, aprendimos cómo realizar selecciones de elementos de listas y arrays Numpy. El procedimiento para seleccionar en tuplas funciona de la misma manera. Considera la siguiente tupla:

```py
carros = (
    (
        'Jetta Variant',
        'Motor 4.0 Turbo',
        2003,
        False,
        ('Llantas de aleación', 'Cerraduras eléctricas', 'Piloto automático')
    ),
    (
        'Passat',
        'Motor Diesel',
        1991,
        True,
        ('Central multimedia', 'Techo panoramico', 'Frenos ABS')
    )
)
```

Considera también los siguientes códigos de selección:

1. carros[0][3]
2. carros[-1][-1][-1]
3. carros[0][-1][:2]

Selecciona la alternativa que muestra el resultado obtenido con los códigos de selección anteriores.

Rta.

1. False
2. 'Frenos ABS'
3. ('Llantas de aleación', 'Cerraduras eléctricas')

Es la misma técnica que aprendimos con listas y arrays Numpy.

### **Bucles for con tuplas**

El procedimiento de iteración en tuplas es el mismo que aprendimos con listas, en el entrenamiento anterior. Usamos la tupla como iterador de un bucle for simple o anidado, y obtenemos acceso a cada elemento individualmente.

Para responder a esta pregunta, considera la misma tupla de la actividad anterior:

```py
carros = (
    (
        'Jetta Variant',
        'Motor 4.0 Turbo',
        2003,
        False,
        ('Llantas de aleación', 'Cerraduras eléctricas', 'Piloto automático')
    ),
    (
        'Passat',
        'Motor Diesel',
        1991,
        True,
        ('Central multimedia', 'Techo panoramico', 'Frenos ABS')
    )
)
```

Observa que se trata de una tupla (primer nivel) con dos tuplas, que representan un conjunto de datos de dos vehículos (segundo nivel), y que una de estas informaciones (accesorios) también viene dentro de una tupla (tercer nivel). Lo que necesitamos es iterar la tupla carros e imprimir todos los accesorios que aparezcan. El resultado deseado es el siguiente:

```txt
Llantas de aleación
Cerraduras eléctricas
Piloto automático
Central multimedia
Techo panorámico
Frenos ABS
```

Selecciona la alternativa con el código que produce este resultado.

Rta.

```py
for tupla in carros:
    for item in tupla[-1]:
        print(item)
```

Observa que, en el segundo bucle for, usamos las listas de accesorios como iterador.

### **La función zip()**

Dos herramientas muy utilizadas al iterar con tuplas son el **desempaquetado de tuplas** y la built-in function _zip()_.

Con el desempaquetado de tuplas, es posible hacer declaraciones conjuntas de variables y usar cada variable individualmente. Por ejemplo:

Para responder a esta pregunta, considera la misma tupla de la actividad anterior:

```py
nombre, valor = ('Passat', 100000.0)
```

La función zip() permite generar un iterador de tuplas, como en el siguiente ejemplo:

**In [1]:**

```py
nombres = ['Passat', 'Crossfox']
valores = [100000.0, 75000.0]
list(zip(nombres, valores))
```

**Out [1]:**

```py
[('Passat', 100000.0), ('Crossfox', 75000.0)]
```

Considerando las dos listas siguientes:

```py
nombres = ['Passat', 'Crossfox', 'DS5', 'C4', 'Jetta']
kms = [15000, 12000, 32000, 8000, 50000]
```

Y utilizando las herramientas presentadas anteriormente, selecciona la alternativa con el código que permite imprimir los nombres de los vehículos con kilometraje inferior a 20.000 km.

Rta.

```py
for nombre, km in zip(nombres, kms):
    if(km < 20000):
        print(nombre)
```

Observa que, en la construcción del bucle for (primera línea del código), usamos el desempaquetado de tuplas y la función zip(). El output de este código debería ser el siguiente:

```txt
Passat
Crossfox
C4
```

---

## **Mapeo de datos: Diccionarios**

### **Definiciones de diccionarios**

Acerca de listas y mapeos, evalúa las siguientes declaraciones:

1. Listas son estructuras de datos que representan un tipo de mapeo
2. Mapeos son colecciones secuenciales ordenadas, que utilizan índices para acceder a sus valores.

¿Cuál(es) afirmación(es) es(son) correcta(s)?

Rta.

Sólo la afirmación 1 es correcta. La afirmación 2 muestra una definición más cercana a una lista de Python.

### **Forma simple de crear diccionarios**

En nuestra última clase vimos algunas formas de crear un diccionario.

Basado en las dos listas siguientes:

```py
nombres = ['Passat', 'Crossfox', 'DS5', 'C4', 'Jetta']
kms = [15000, 12000, 32000, 8000, 50000]
```

Selecciona la alternativa que muestra una de las formas de crear diccionarios a partir de estas listas.

Rta.

```
dict(zip(nombres, kms))
```

Con la función _zip()_, conseguimos crear diccionarios de forma simple y con poco código.

### **Operaciones básicas con diccionarios**

Utiliza el diccionario datos para responder la pregunta:

```py
datos = {
    'Passat': {
        'año': 2012,
        'km': 50000,
        'valor': 75000,
        'accesorios': ['Airbag', 'ABS']
    },
    'Crossfox': {
        'año': 2015,
        'km': 35000,
        'valor': 25000
    }
}
```

Observa que, datos tiene dos elementos, donde la llave (key) es el nombre del vehículo y el valor (value) es un diccionario con las informaciones sobre este vehículo (año, kilometraje, valor y accesorios). Nuestra tarea en esta actividad es aprender a acceder a las informaciones de un diccionario dentro de otro diccionario.

Queremos lo siguiente: **1)** Testar si la llave accesorios existe en el diccionario de las informaciones del vehículo Crossfox (Respuesta esperada: False) **2)** Testar si la llave accesorios existe en el diccionario de las informaciones del vehículo Passat (Respuesta esperada: True) **3)** Obtener el valor del vehículo Crossfox (Respuesta esperada: 25000) **4)** Acceder el último accesorio del vehículo Passat (Respuesta esperada: 'ABS')

Selecciona la alternativa que muestra los códigos correctos para devolver y acceder a la información anterior (Consejo: utilice un notebook para comprobar los códigos de esta actividad):

Rta.

1. 'accesorios' in datos['Crossfox']
2. 'accesorios' in datos['Passat']
3. datos['Crossfox']['valor']
4. datos['Passat']['accesorios'][-1]

Observa que es muy simple acceder a las informaciones de un diccionario dentro de otro diccionario. Primero, necesitamos acceder a la llave que tiene como valor un diccionario y luego simplemente acceder a la llave que queremos.

### **Conociendo los métodos de diccionarios**

Considera el siguiente diccionario:

```py
datos = {'Jetta': 88000, 'Crossfox': 72000, 'DS5': 124000}
```

Juzga las siguientes alternativas y marque las correctas.

Rta.

- El método copy() crea una copia del diccionario. Funciona de la misma manera que aprendimos en el curso anterior, cuando hablamos de listas de Python.

- El método update() actualiza el diccionario y se puede utilizar de dos formas:
  - In [1]:
  ```py
  datos.update({'Passat': 85000, 'Fusca': 150000})
  ```
  - Out [1]:
  ```py
  {'Jetta': 88000, 'Crossfox': 72000, 'DS5': 124000, 'Passat': 85000, 'Fusca': 150000}
  ```
  - In [2]:
  ```py
      datos.update(Passat = 95000, Fusca = 160000)
  ```
  - Out [2]:
  ```py
  {'Jetta': 88000, 'Crossfox': 72000, 'DS5': 124000, 'Passat': 95000, 'Fusca': 160000}
  ```

Observa que, si la llave tuviera un nombre no válido para las variables de Python (por ejemplo: 'Jetta Variant'), el segundo formato no podría ser utilizado.

### **Iterando en diccionarios de diccionarios**

Utiliza el siguiente diccionario para responder a esta actividad:

```py
datos = {
    'Crossfox': {'valor': 72000, 'año': 2005},
    'DS5': {'valor': 125000, 'año': 2015},
    'Fusca': {'valor': 150000, 'año': 1976},
    'Jetta': {'valor': 88000, 'año': 2010},
    'Passat': {'valor': 106000, 'año': 1998}
}
```

Selecciona la alternativa que muestre el código que imprima sólo los nombres de los vehículos que tengan el año de fabricación mayor o igual a 2000.

Rta.

```py
for item in datos.items():
    if(item[1]['año'] >= 2000):
        print(item[0])
```

Observa que datos.items() devuelve un iterador de tuplas, donde cada tupla tiene como primer ítem la llave del diccionario y como segundo ítem su respectivo valor.

---

## **Organización del código: Funciones y paquetes**

### **Fijando el conocimiento**

En nuestros dos entrenamientos de **Python para Data Science**, conocimos algunas funciones y tipos integrados del lenguaje. Las _built-in functions_ siempre están disponibles, es decir, no es necesario importarlas cuando se utilizan. Podemos encontrar una lista de ellas en la siguiente dirección:

[https://docs.python.org/3.6/library/functions.html](https://docs.python.org/3.6/library/functions.html "Documentation").

Entre las alternativas a continuación, selecciona las que tienen una _built-in function_ y su respectiva definición correctas.

Rta.

```py
dict() # Crea un diccionario
```

- En nuestras últimas clases, usamos la función _dict()_ para crear diccionarios.

```py
zip() # Devuelve un iterador de tuplas
```

- La función _zip()_ es muy útil cuando necesitamos iterar en más de una secuencia al mismo tiempo. También se usa ampliamente junto con la función _dict()_ para crear diccionarios.

```py
type() # Devuelve el tipo de un objeto
```

- Usamos esta _built-in function_ varias veces en nuestro entrenamiento. Con ella, podemos identificar con qué tipos de datos estamos trabajando.

### **Kilometraje promedio de un vehículo**

Una tarea importante en el trabajo de un científico de datos es el tratamiento del dataset que vamos a utilizar. Esta tarea se divide en varias etapas y una de ellas es la **sumarización de los datos**, donde obtenemos estadísticas descriptivas para ayudar en la toma de decisiones.

Algunas sumarizaciones necesitan un conjunto de elaboraciones, que deben ser desarrolladas por el propio investigador. En este punto, las funciones son bastante útiles en el trabajo del científico de datos, con ellas, podemos definir un conjunto de tareas específicas, que reciben entradas y devuelven resultados, y reutilizan esta codificación en otras partes de nuestro proyecto.

Un ejemplo de esto en nuestro proyecto es el cálculo del kilometraje promedio que recorre un vehículo por año. No hay una función lista para calcular esta estadística en Python, por lo que necesitamos escribir una función que obtenga este valor.

Considera el conjunto de informaciones a continuación para responder el problema:

```py
KMmedia = KMtotal / (AÑOactual - AÑOfabricación)
```

```py
datos = {
    'Crossfox': {'km': 35000, 'año': 2005},
    'DS5': {'km': 17000, 'año': 2015},
    'Fusca': {'km': 130000, 'año': 1979},
    'Jetta': {'km': 56000, 'año': 2011},
    'Passat': {'km': 62000, 'año': 1999}
}
```

La siguiente estructura es la definición de una función que obtiene e imprime el kilometraje promedio anual de cada vehículo en un diccionario con la estructura del diccionario datos:

In [1]:

```py
def km_media(dataset, año_actual):
    for item in _______________:
        result = _____________ / (año_actual - ____________)
        print(result)
```

In [2]:

```py
km_media(datos, 2019)
```

Out [2]:

```py
2500.0
4250.0
3250.0
7000.0
3100.0
```

Selecciona la alternativa que complete correctamente los vacíos de nuestra función.

Rta.

```py
def km_media(dataset, año_actual):
    for item in dataset.items():
        result = item[1]['km'] / (año_actual - item[1]['año'])
        print(result)
```

Todavía tenemos que mejorar un poco esta función. Haremos esto en las próximas actividades.

### **Mejorando nuestra función**

En el problema anterior, definimos una función para obtener el kilometraje promedio anual de cada vehículo en un dataset. Necesitamos mejorar un poco nuestra función y obtener valores que se puedan reutilizar en otras partes de nuestro proyecto.

Aprendimos en nuestro último video cómo crear funciones que devuelvan valores, y eso es lo que necesitamos para resolver este problema. La siguiente estructura es la definición de una función que calcula el kilometraje promedio anual de cada vehículo y devuelve un diccionario con los nombres de los vehículos como llaves y el kilometraje promedio como valores:

In [1]:

```py
datos = {
    'Crossfox': {'km': 35000, 'año': 2005},
    'DS5': {'km': 17000, 'año': 2015},
    'Fusca': {'km': 130000, 'año': 1979},
    'Jetta': {'km': 56000, 'año': 2011},
    'Passat': {'km': 62000, 'año': 1999}
}
```

In [2]:

```py
def km_media(dataset, año_actual):
    result = {}
    for item in dataset.items():
        media = item[1]['km'] / (año_actual - item[1]['año'])
        _________________________________
    ________________
```

In [3]:

```py
km_media(datos, 2019)
```

Out [3]:

```py
{'Crossfox': 2500.0,
 'DS5': 4250.0,
 'Fusca': 3250.0,
 'Jetta': 7000.0,
 'Passat': 3100.0}
```

Selecciona la alternativa que complete correctamente los vacíos de nuestra función.

Rta.

```py
def km_media(dataset, año_actual):
    result = {}
    for item in dataset.items():
        media = item[1]['km'] / (año_actual - item[1]['año'])
        result.update({ item[0]: media })
    return result
```

Ahora tenemos una función un poco más elaborada.

### **Mejorando un poco más nuestra función**

Nuestra función km_media() ya devuelve valores que pueden ser utilizados en otras partes de nuestro proyecto. Una elaboración extra, que también puede ser interesante, principalmente para la creación de DataFrames (próximas clases), es la actualización del propio input de la función. Podemos hacer que nuestra función devuelva las informaciones del diccionario datos, incluyendo las informaciones del kilometraje promedio anual.

La siguiente estructura es la definición de una función que calcula el kilometraje promedio anual de cada vehículo, actualiza el diccionario de entrada y devuelve este diccionario:

In [1]:

```py
datos = {
    'Crossfox': {'km': 35000, 'año': 2005},
    'DS5': {'km': 17000, 'año': 2015},
    'Fusca': {'km': 130000, 'año': 1979},
    'Jetta': {'km': 56000, 'año': 2011},
    'Passat': {'km': 62000, 'año': 1999}
}
```

In [2]:

```py
def km_media(dataset, año_actual):
    result = {}
    for item in dataset.items():
        media = item[1]['km'] / (año_actual - item[1]['año'])
        ____________________________________
        ___________________________________

    return result
```

In [3]:

```py
km_media(datos, 2019)
```

Out [3]:

```py
{'Crossfox': {'km': 35000, 'año': 2005, 'km_media': 2500.0},
 'DS5': {'km': 17000, 'año': 2015, 'km_media': 4250.0},
 'Fusca': {'km': 130000, 'año': 1979, 'km_media': 3250.0},
 'Jetta': {'km': 56000, 'año': 2011, 'km_media': 7000.0},
 'Passat': {'km': 62000, 'año': 1999, 'km_media': 3100.0}}
```

Selecciona la alternativa que complete correctamente los vacíos de nuestra función.

Rta.

```py
def km_media(dataset, año_actual):
    result = {}
    for item in dataset.items():
        media = item[1]['km'] / (año_actual - item[1]['año'])
        item[1].update({ 'km_media': media })
        result.update({ item[0]: item[1] })
    return result
```

Ahora tenemos una función más completa. Con el resultado de esta función, podemos crear DataFrames, como veremos en las siguientes clases y actividades.

---

## **La biblioteca de los científicos de datos: Pandas**

### **Pandas y sus estructuras de datos**

Acerca de Pandas y sus estructuras de datos, evalúa las siguientes afirmaciones:

1. Pandas es una herramienta de manipulación de datos de alto nivel, construida sobre la base del paquete Numpy
2. Series son arrays unidimensionales, capaces de almacenar sólo un tipo de dato
3. DataFrame es una estructura de datos tabular bidimensional con rótulos en las filas y en las columnas

¿Qué afirmaciones son correctas?

Rta.

Las afirmaciones 1 y 3 son correctas. Series y DataFrames pueden almacenar cualquier tipo de dato.

### **Creando DataFrames**

Vimos en el último video que es posible crear DataFrames y Series a partir de varias fuentes (archivos externos, listas, diccionarios, etc.).

En la clase que hablamos sobre funciones, desarrollamos en nuestros ejercicios una función que recibía un diccionario con un conjunto de informaciones sobre vehículos, y calculaba el kilometraje promedio anual de cada vehículo. Esta función devolvía el contenido del diccionario de input de la función, más las informaciones sobre el kilometraje promedio:

**In [1]:**

```py
datos = {
    'Crossfox': {'km': 35000, 'año': 2005},
    'DS5': {'km': 17000, 'año': 2015},
    'Fusca': {'km': 130000, 'año': 1979},
    'Jetta': {'km': 56000, 'año': 2011},
    'Passat': {'km': 62000, 'año': 1999}
}
```

**In [2]:**

```py
def km_media(dataset, año_actual):
    result = {}
    for item in dataset.items():
        media = item[1]['km'] / (año_actual - item[1]['año'])
        item[1].update({ 'km_media': media })
        result.update({ item[0]: item[1] })

    return result
```

**In [3]:**

```py
km_media(datos, 2019)
```

**Out [3]:**

```py
{'Crossfox': {'km': 35000, 'año': 2005, 'km_media': 2500.0},
 'DS5': {'km': 17000, 'año': 2015, 'km_media': 4250.0},
 'Fusca': {'km': 130000, 'año': 1979, 'km_media': 3250.0},
 'Jetta': {'km': 56000, 'año': 2011, 'km_media': 7000.0},
 'Passat': {'km': 62000, 'año': 1999, 'km_media': 3100.0}}
```

Selecciona la alternativa que muestra la forma correcta de crear un DataFrame con el resultado obtenido por la función anterior. El DataFrame resultante debe tener la siguiente forma:

**Out [1]:**

|          | km       | año    | km_media |
| -------- | -------- | ------ | -------- |
| Crossfox | 35000.0  | 2005.0 | 2500.0   |
| DS5      | 17000.0  | 2015.0 | 4250.0   |
| Fusca    | 130000.0 | 1979.0 | 3250.0   |
| Jetta    | 56000.0  | 2011.0 | 7000.0   |
| Passat   | 62000.0  | 1999.0 | 3100.0   |

Consejo: Para resolver este problema, será necesario recordar un recurso que aprendimos en el curso ant1!|erior, cuando hablamos de arrays Numpy. Este recurso también se puede aplicar a DataFrames de pandas:

```py
ndarray.T: Devuelve la matriz transpuesta, es decir, convierte filas en columnas y viceversa.
```

Rta.

```py
import pandas as pd
carros = pd.DataFrame(km_media(datos, 2019)).T
```

La propiedad T es una forma de acceder al método transpose() del DataFrame.

### **Particiones con DataFrames**

Considera el siguiente código:

import pandas as pd

```py
datos = {
   'Nombre': ['Jetta', 'Passat', 'Crossfox', 'DS5', 'Fusca'],
   'Motor': ['Motor 4.0 Turbo', 'Motor Diesel', 'Motor Diesel V8', 'Motor 2.0', 'Motor 1.6'],
    'Año': [2019, 2003, 1991, 2019, 1990],
    'Kilometraje': [0.0, 5712.0, 37123.0, 0.0, 120000.0],
    'Cero_km': [True, False, False, True, False],
    'Valor': [88000.0, 106000.0, 72000.0, 89000.0, 32000.0]
}

dataset = pd.DataFrame(datos)
```

Elige la alternativa que contenga el código que selecciona solamente las informaciones de Nombre, Año, Kilometraje y Valor de los vehículos Passat y Crossfox.

Rta.

```py
dataset[['Nombre', 'Año', 'Kilometraje', 'Valor']][1:3]
```

Observa que, el orden en el que definimos las selecciones es indiferente:

```py
dataset[1:3][['Nombre', 'Año', 'Kilometraje', 'Valor']]
```

El código anterior devuelve el mismo resultado.

### **Utilizando .loc y .iloc para selecciones**

Utiliza el siguiente código para resolver el problema:

```py
import pandas as pd

datos = {
   'Motor': ['Motor 4.0 Turbo', 'Motor Diesel', 'Motor Diesel V8', 'Motor 2.0', 'Motor 1.6'],
   'Año': [2019, 2003, 1991, 2019, 1990],
   'Kilometraje': [0.0, 5712.0, 37123.0, 0.0, 120000.0],
   'Cero_km': [True, False, False, True, False],
   'Valor': [88000.0, 106000.0, 72000.0, 89000.0, 32000.0]
}

dataset = pd.DataFrame(datos, index = ['Jetta', 'Passat', 'Crossfox', 'DS5', 'Fusca'])
```

Observa que usamos los nombres de los vehículos como índice del DataFrame. Selecciona las alternativas correctas que tienen como resultado el siguiente DataFrame:

**Out [1]:**

|        | Motor       | Valor    |
| ------ | ----------- | -------- |
| Passat | Motor disel | 106000.0 |
| DS5    | Motor 2.0   | 89000.0  |

Rta.

```py
dataset.iloc[[1, 3], [0, -1]]
```

- Usando .iloc, las selecciones se hacen por la posición de las filas y las columnas, es decir, usando los índices numéricos del DataFrame.

```py
dataset.loc[['Passat', 'DS5'], ['Motor', 'Valor']]
```

Usando .loc, podemos hacer las selecciones usando los rótulos de las filas y las columnas de un DataFrame.

### **Realizando consultas en un DataFrame**

Utiliza el siguiente DataFrame para resolver el problema:

```py
import pandas as pd

datos = {
  'Motor': ['Motor 4.0 Turbo', 'Motor Diesel', 'Motor Diesel V8', 'Motor Diesel', 'Motor 1.6'],
    'Año': [2019, 2003, 1991, 2019, 1990],
    'Kilometraje': [0.0, 5712.0, 37123.0, 0.0, 120000.0],
    'Cero_km': [True, False, False, True, False],
    'Valor': [88000.0, 106000.0, 72000.0, 89000.0, 32000.0]
}

dataset = pd.DataFrame(datos, index = ['Jetta', 'Passat', 'Crossfox', 'DS5', 'Fusca'])
```

Queremos devolver el siguiente DataFrame:

**Out [1]:**

|        | Motor           | Año  | Kilometraje | Cero_km | Valor    |
| ------ | --------------- | ---- | ----------- | ------- | -------- |
| Jetta  | Motor 4.0 Turbo | 2019 | 0.0         | True    | 88000.0  |
| Passat | Motor Diesel    | 2003 | 5712.0      | False   | 106000.0 |
| DS5    | Motor Diesel    | 2019 | 0.0         | True    | 89000.0  |

De las alternativas, selecciona las que tienen el código que devuelve el DataFrame anterior.

Rta.

```py
dataset.query('Motor == "Motor Diesel" | Cero_km == True')
```

- El método query() acepta las 2 formas de utilización de operadores lógicos. En el caso del operador lógico O, tenemos el or y el carácter |. Para el operador lógico Y, tenemos el and y el carácter &.

```py
dataset[(dataset.Motor == 'Motor Diesel') | (dataset.Cero_km == True)]
```

- En la construcción de la matriz booleana que utilizamos para realizar la consulta, fue utilizado el operador lógico O, que en el caso del código anterior está representado por el carácter.

```py
dataset.query('Motor == "Motor Diesel" or Cero_km == True')
```

- El método query() acepta las 2 formas de utilización de operadores lógicos. En el caso del operador lógico O, tenemos el or y el carácter |. Para el operador lógico Y, tenemos el and y el carácter &.

### **Para saber más: Formas de iterar en un DataFrame**

En nuestro último video, aprendimos una forma de iterar en un DataFrame. Esa no es la única forma de realizar esta tarea.

Selecciona las alternativas que muestran formas de iterar en un DataFrame.

Sugerencia: Como este contenido no lo vimos en clase, usa la documentación de pandas [https://pandas.pydata.org/pandas-docs/stable/](https://pandas.pydata.org/pandas-docs/stable/ "Documentation") para encontrar informaciones sobre estos nuevos métodos. También usa un notebook para probar los códigos.

Rta.

```py
itertuples()
```

- Este método itera a través de las líneas del DataFrame, usando una tupla nombrada con el contenido de cada línea del DataFrame. Revisa la documentación: [https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.itertuples.html](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.itertuples.html "Documentation")

```py
iterrows()
```

- Este fue el método que vimos en nuestra clase. Consulta la documentación para obtener más información: [https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.iterrows.html](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.iterrows.html "Documentation")

```py
items()
```

- Itera a través de las columnas del DataFrame, usando una tupla, donde el primer elemento es el nombre de la columna y el segundo es una Series, con todo el contenido de la columna. Más información en: [https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.items.html](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.items.html "Documentation")

### **Identificando y tratando datos faltantes**

En el último video, conocimos algunas de las herramientas que proporciona el paquete pandas para identificar y tratar la información faltante en un DataFrame.

Sobre esto, evalúa las siguientes afirmaciones:

1. **isna()**: Detecta valores faltantes. Devuelve un DataFrame o una Series booleana, identificando si el registro es un NA
2. **fillna()**: Llena los registros identificados con NA, utilizando un método específico
3. **dropna()**: Elimina los valores identificados como NA

¿Qué afirmaciones son correctas?

Rta.

Todas las afirmaciones son correctas. Recuerda que el tipo de tratamiento que aplicamos a nuestros datos debe tener siempre en cuenta los objetivos de nuestro estudio.

### **Lo que aprendimos**

En esta aula aprendimos:

- Series y DataFrames, las estructuras de datos básicas de la biblioteca pandas.
- Los métodos para crear Series y DataFrames.
- A crear DataFrames a partir de datos externos.
- Cómo hacer selecciones y cortes en un DataFrame.
- Los métodos de selección .loc y .iloc.
- A realizar consultas en un DataFrame con el método .query() y con matrices booleanas.
- La forma básica de iterar sobre un DataFrame.
- Cómo tratar datos faltantes en un DataFrame (isna(), fillna() y dropna()).
