# **Oracle Analytics - Visualizando datos**

## **Oracle Analytics**

### **La importancia de la visualización**

En nuestra aula, vimos que Penélope escogió el software de **Oracle Analytics** para ayudar en el desarrollo de los análisis acerca del Club del Libro. Con este software, aprendemos a trabajar con los principios de visualización de datos y cómo estos nos pueden auxiliar en los análisis que tenemos en nuestras manos.

Considerando lo que estudiamos hasta el momento, podemos utilizar la visualización de datos para:

Rta.

- Crear gráficos personalizados. Como la visualización de datos busca adecuar el contenido a las demandas del cliente, la personalización es uno de los atributos más atractivos agregando valor y significado a los mismos.
- Construir una historia basada en los datos. La capacidad de contar historias con los datos es una de las principales motivaciones para el uso de la visualización de datos.
- Tomar decisiones con base en las visualizaciones. Así es, los números transformados en visualizaciones pueden ser más rápidos de asimilar y facilitan la toma de decisiones.

### **¿Por dónde comenzar?**

Juan es un estudiante de ciencias de la computación que se interesa mucho por la ciencia de datos. Él ya participó en algunas charlas sobre visualización de datos y se ve en el futuro actuando en esta área. Para practicar el contenido, Juan separó una base de datos y pensó en algunos gráficos, pero aún no sabe cómo continuar sus estudios.

¿Cuál es la mejor práctica para ayudar a Juan en sus estudios?

Elaborar un portafolio con sus primeras visualizaciones e insights. Así es, hay muchas técnicas para elaborar buenas visualizaciones, pero no es necesario saberlas todas para comenzar a colocarlas en práctica en sus estudios. Elaborar un portafolio con sus visualizaciones y compartir sus insights es una buena manera de registrar sus conocimientos y su evolución en el asunto.

### **Para saber más: cómo utilizar Oracle Analytics en la nube**

Tenemos la posibilidad de trabajar con **Oracle Analytics** directamente desde la nube. Inclusive, podemos llamarlo **Oracle Analytics Cloud**, cuando trabajamos con esta herramienta de esta manera.

Existen algunas ventajas de trabajar con el software directamente desde la nube, por ejemplo: No tenemos que realizar la instalación en nuestra máquina y siempre estará en su versión más actualizada. Adicionalmente, contaremos con todos los recursos disponibles, como el recurso **Explicar**.

Para utilizar **Oracle Analytics Cloud** podemos acceder a este [link](https://www.oracle.com/cloud/sign-in.html?redirect_uri=https%3A%2F%2Fcloud.oracle.com%2F%3Fregion%3Dsa-saopaulo-1).

![Como utilizar Oracle Analytics en la nube 1](./img/como-utilizar-Oracle-Analytics-en-la-nube-1.jpg)

Ahora, necesitas acceder con tu login y contraseña de Oracle, para entrar a Oracle Cloud. En caso de que no tengas una cuenta, debes crear una en el botón **Sign Up** en la parte inferior.

**Observación**: Debes registrarte con una tarjeta de crédito para poder crear tu cuenta de Cloud. Inicialmente tendrás acceso a recursos gratuitos, pero si decides hacer un upgrade a tu cuenta, entonces sí te van a cobrar.

Después de ello, seremos redireccionados a la siguiente página:

![Como utilizar Oracle Analytics en la nube 2](./img/como-utilizar-Oracle-Analytics-en-la-nube-2.jpg)

Nos desplazaremos al menú de sándwich en la esquina superior izquierda de nuestra pantalla y haremos clic sobre: **Analytics & IA**:

![Como utilizar Oracle Analytics en la nube 3](./img/como-utilizar-Oracle-Analytics-en-la-nube-3.jpg)

Hacemos clic en la opción: **Analytics Cloud**.

![Como utilizar Oracle Analytics en la nube 4](./img/como-utilizar-Oracle-Analytics-en-la-nube-4.jpg)

En este punto podemos crear la instancia en que **Oracle Analytics** se va a ejecutar.

![Como utilizar Oracle Analytics en la nube 5](./img/como-utilizar-Oracle-Analytics-en-la-nube-5.jpg)

Harás clic sobre **Create instance**:

![Como utilizar Oracle Analytics en la nube 6](./img/como-utilizar-Oracle-Analytics-en-la-nube-6.jpg)

Vamos a llenar los campos requeridos.

En este punto, cómo utilizaremos la instancia de manera personal, estableceremos las opciones de capabilities como **Self-Service Analytics**, **OCPU** y **1**.

![Como utilizar Oracle Analytics en la nube 7](./img/como-utilizar-Oracle-Analytics-en-la-nube-7.jpg)

Harás clic sobre **Create**:

![Como utilizar Oracle Analytics en la nube 8](./img/como-utilizar-Oracle-Analytics-en-la-nube-8.jpg)

Ahora, esperamos mientras la instancia es creada:

![Como utilizar Oracle Analytics en la nube 9](./img/como-utilizar-Oracle-Analytics-en-la-nube-9.jpg)

**Nota**: Durante nuestros ensayos, algunas veces ese tiempo duró entre 10 y 20 minutos. Entonces, vamos a esperar hasta que todo quede listo y la instancia sea creada.

![Como utilizar Oracle Analytics en la nube 10](./img/como-utilizar-Oracle-Analytics-en-la-nube-10.jpg)

El próximo paso es hacer clic sobre **Analytics Home Page**.

![Como utilizar Oracle Analytics en la nube 11](./img/como-utilizar-Oracle-Analytics-en-la-nube-11.jpg)

¡Te felicito! Ya puedes utilizar tu **Oracle Analytics Cloud**.

### **Haga lo que hicimos en aula**

Realiza el download de **Oracle Analytics** en la [página oficial](https://www.oracle.com/middleeast/solutions/business-analytics/analytics-desktop/oracle-analytics-desktop.html#),haciendo clic en **download** en el producto **Oracle Analytics Desktop**, Como se observa en la siguiente imagen:

![Oracle Analytics 1](./img/Oracle-Analytics-1.jpg)

En seguida, es necesario que hagas login en tu cuenta de Oracle. En caso de que no tengas una cuenta, podrás crear una gratuitamente escogiendo la opción **Crear Cuenta**, como se muestra en la siguiente figura. Registra tus datos y, a continuación, la cuenta será creada. Para quien ya posee una cuenta, el nombre de usuario, generalmente, es el e-mail empleado durante el registro.

![Oracle Analytics 2](./img/Oracle-Analytics-2.jpg)

Tan pronto efectúes el login, aparecerá una página semejante a la de la siguiente imagen. En esta página, necesitas escoger tu sistema operativo y luego seleccionar la opción **I reviewed and accept the Oracle License Agreement** para aceptar los términos y condiciones de uso del software.

![Oracle Analytics 3](./img/Oracle-Analytics-3.jpg)

Ahora, solo debes hacer clic en **Download** y esperar la conclusión del proceso que generará el ejecutable para la instalación del software en tu máquina.

![Oracle Analytics 4](./img/Oracle-Analytics-4.jpg)

Abrimos el ejecutable en nuestra máquina para realizar la instalación del **Oracle Download Manager** que solicitará el directorio donde queremos almacenar el archivo comprimido (.zip) que contiene el ejecutable para la instalación del software **Oracle Analytics Desktop**. Luego de escoger el directorio de almacenamiento, debemos dar clic en **Next** para continuar con la instalación, como se muestra a continuación:

![Oracle Analytics 5](./img/Oracle-Analytics-5.jpg)

Después de completar el download haremos clic en el ícono de carpeta en la esquina inferior izquierda para acceder al archivo (.zip) que fue descargado.

![Oracle Analytics 6](./img/Oracle-Analytics-6.jpg)

En caso que hayas cerrado la ventana, es posible acceder el archivo (.zip) en el directorio donde quedó almacenado. Al localizar este archivo, procederemos a descompactarlo para la instalación del **Oracle Analytics Desktop**.

![Oracle Analytics 7](./img/Oracle-Analytics-7.jpg)

Luego de descompactar el archivo, obtendremos un nuevo ejecutable. Para instalar **Oracle Analytics** debemos ejecutarlo, seleccionar la opción **Launch after install**, para que el software se pueda iniciar tan pronto como termine el download, y hagas clic en **Install**.

![Oracle Analytics 8](./img/Oracle-Analytics-8.jpg)

Después harás clic en **Finish**.

![Oracle Analytics 9](./img/Oracle-Analytics-9.jpg)

A partir de este memento, **Oracle Analytics Desktop** está listo para ser utilizado.

![Oracle Analytics 10](./img/Oracle-Analytics-10.jpg)

Lo que aprendimos

- Estructurar los principales puntos antes de iniciar un proyecto;
- Utilizar el software de Oracle Analytics en la toma de decisiones a través de la creación de visualizaciones;
- Instalar el software de Oracle Analytics.

---

## **Creando el proyecto**

### **Desafío: tipos de conexiones**

¡Te propongo un desafío! Puedes crear nuevos tipos de conexiones utilizando otras bases de datos.

Siéntete libre de probar los diferentes tipos posibles de conexiones dentro de **Oracle Analytics**. Puedes conectar archivos que ya haya en tu ambiente o inclusive realizar la conexión con bancos de datos, como aquellos de los demás cursos disponibles en Alura.

Además, puedes acceder a [kaggle](https://www.kaggle.com/), que es una plataforma gratuita que coloca a disposición varios datasets gratuitos para practicar tus conocimientos.

### **Tipos de datos**

Al realizar el proceso de transformación y tratamiento de los datos, Penélope notó la importancia de categorizar correctamente las columnas de las tablas en atributos o medidas para lograr trabajar de la mejor manera con los datos. Con base en tu conocimiento, clasifica las columnas de la siguiente tabla en atributos o medidas:

| ID  | Descripción          | Embalaje | Precio | Costo |
| --- | -------------------- | -------- | ------ | ----- |
| 1   | Sandía Fruit Ice     | Bolsita  | 3,5    | 1,17  |
| 2   | Napolitana Cream Ice | Pote     | 22,5   | 7,5   |
| 3   | Uva Fruit Ice        | Bolsita  | 4      | 1,33  |
| 4   | Flocos Fruit Ice     | Bolsita  | 4,5    | 1,5   |
| 5   | Limón Fruit Ice      | Bolsita  | 2,5    | 0,83  |

Rta.

```txt
ID - Atributo;
Descripción - Atributo;
Embalaje - Atributo;
Precio - Medida;
Costo - Medida.
```

Una medida permite que realicemos cálculos y los apliquemos en nuestras visualizaciones. Ya los atributos están más relacionados a las categorías.

### **Para saber más: Augmented Analytics**

**Augmented Analytics** o **Análisis aumentado** es una tecnología que emplea la **Inteligência Artificial (IA)** y técnicas de **Machine Learning** para automatizar el proceso de análisis de datos realizado con las herramientas analíticas.

Algunos de los procesos mediante los cuales esta tecnología nos auxilia son en la exploración, identificación y preparación de los datos, generando insights que aumentan la forma como exploramos y analizamos estos datos. Dicha automación disminuye, o incluso, hasta sustituye el tiempo dedicado a la organización y el análisis de los datos realizado por los profesionales.

Es como si la máquina tuviese el poder de leer mentes y presentase exactamente lo que se necesita saber antes de que los profesionales analicen los datos propiamente.

Las organizaciones que comiencen a utilizar esta tecnología tienen el potencial de ganar una ventaja competitiva en el mercado y se cree que prontamente **Augmented Analytics** será esencial para cualquier proyecto de BI.

### **Haga lo que hicimos en aula**

Llegó el momento de colocar en práctica tus conocimientos, transformando y tratando algunos datos. Iniciaremos creando una nueva conexión para importar nuestra base de datos. Para ello, haz clic en la opción **Crear** localizada en la esquina superior derecha de tu pantalla.

![Creando el proyecto 1](./img/creando-el-proyecto-1.jpg)

Selecciona la opción Conjunto de datos o, en caso que ya hayas importado los archivos con los cuales trabajaremos, selecciona la opción **Directorio de Trabajo**.

![Creando el proyecto 2](./img/creando-el-proyecto-2.jpg)

Para realizar la importación de la base de datos, selecciona el archivo y arrástralo hasta la nueva ventana, o haz clic en el ícono y busca mediante el directorio donde el archivo se encuentra almacenado en tu ambiente:

![Creando el proyecto 3](./img/creando-el-proyecto-3.jpg)

Al cargar los datos, altera el nombre del archivo y los separadores de casillas decimales.

![Creando el proyecto 4](./img/creando-el-proyecto-4.jpg)

Después de adicionar el archivo importado, una nueva pantalla estará disponible para poder realizar el tratamiento de los datos.

![Creando el proyecto 5](./img/creando-el-proyecto-5.jpg)

Al lado izquierdo de la pantalla, en el área de Recomendaciones, encontrarás una serie de sugerencias que el mismo software identifica y que nos permiten realizar un tratamiento a los datos:

![Creando el proyecto 6](./img/creando-el-proyecto-6.jpg)

En caso de que realices una alteración y no la quieras mantener, es posible excluir esta etapa haciendo click sobre la "x" localizada dentro de la caja de texto con el nombre de la alteración realizada y **Excluir Etapa**.

![Creando el proyecto 7](./img/creando-el-proyecto-7.jpg)

Si no encontramos las alteraciones que deseamos realizar en las recomendaciones, podemos hacerlo de forma manual.

![Creando el proyecto 8](./img/creando-el-proyecto-8.jpg)

### **Lo que aprendimos**

- Crear un directorio de trabajo;
- Realizar conexiones utilizando un archivo _.csv_;
- Transformar y tratar los datos;
- Las diferencias entre medida y atributo;
- Trabajar con scripts utilizando las ayudas de **Augmented Analytics**.

---

## **Creando gráficas**

### **Accediendo a los archivos del aula**

Para que puedas comenzar en esta aula, es necesario que tengas el material específico que el instructor va a utilizar. Puedes hacer el download de este material haciendo clic [aquí](https://caelum-online-public.s3.amazonaws.com/1887-oracle-analytics-visualizando-datos/encuesta.csv).

### **Desafío: formas de crear visualizaciones**

¡Te propongo otro desafío! Después de oír a Penélope hablando sobre las diversas formas de crear visualizações en **Oracle Analytics**, Pablo quedó muy animado y desea crear otros gráficos. ¿Qué tal si ayudamos a Pablo a hacerlo?

Crea un gráfico de barras y explore las columnas disponibles para análisis.

### **Sobre el recurso explicar**

Comentando sobre el proyecto con Pablo, uno de los analistas del equipo, Penélope mencionó el recurso **Explicar**. Esto despertó la curiosidad en él, y por ello le gustaría entender mejor cómo funciona.

De las siguientes opciones, ¿Cuál es la que mejor explica el funcionamiento de este recurso?

Rta.

El recurso **Explicar** analiza la columna seleccionada dentro del contexto del conjunto de datos y genera textos descriptivos sobre los insights encontrados. Con este recurso,podrás descubrir hechos básicos, factores determinantes, segmentos que explican la columna y sus posibles anomalías.

### **Para saber más: cuándo utilizar el gráfico de barras**

El gráfico de barras es muy útil para comparar categorías. Generalmente son usados para evitar el desorden de los datos, cuando las leyendas son muy extensas o si tenemos más de 10 items para comparar.

Dentro del gráfico de barras existen algunos subtipos:

- Barras agrupadas: Los items son agrupados en el eje vertical, permitiendo una comparación rápida de valores.
- Barras apiladas: Una única barra es utilizada para exhibir los items y los colores de las leyendas son separados de manera bien evidente.
- Barras 100% apiladas: Trabajan bajo el mismo concepto de barras apiladas, con la diferencia de que los valores son exhibidos en porcentajes.

**Tips para crear un gráfico de barras:**

- Nombra los ejes.
- Coloca leyendas de valores en las barras.
- Evita gráficos “arco-íris”, o sea, no dejes el gráfico de todos los colores. Es mucho mejor utilizar un solo color o varias tonalidades de un mismo color.

### **Haga lo que hicimos en aula**

Para iniciar con la configuración del recurso Explicar columna, necesitamos dirigirnos hacia el directorio C:\Program Files\Oracle Analytics Desktop.

![Creando graficas 1](./img/creado-graficas-1.jpg)

Luego de acceder al directorio, debes seleccionar el archivo: install_dvml.cmd.

![Creando graficas 2](./img/creado-graficas-2.jpg)

Al hacer doble click en el archivo install_dvml, una pantalla del símbolo del sistema se abre para hacer el download y la instalación del recurso.

![Creando graficas 3](./img/creado-graficas-3.jpg)

Al final de la instalación, un mensaje para reiniciar el computador será exhibido. Después de reiniciar nuestro equipo, el recurso habrá quedado completamente instalado. En Oracle Analytics, harás clic derecho sobre cualquier columna y la opción Explicar estará disponible.

![Creando graficas 4](./img/creado-graficas-4.jpg)

### **Lo que aprendimos**

- Crear gráficos;
- Instalar el recurso explicar columna;
- Generar gráfico de barras, de columnas y de barras apiladas.

---

## **Interacción entre las visualizaciones**

### **Accediendo a los archivos del aula**

Para que puedas comenzar en esta aula, es necesario que tengas el material específico que el instructor va a utilizar. Puedes hacer el download de este material haciendo clic [aquí](https://caelum-online-public.s3.amazonaws.com/1887-oracle-analytics-visualizando-datos/compras_de_clientes.csv), [aquí](https://caelum-online-public.s3.amazonaws.com/1887-oracle-analytics-visualizando-datos/clientes.csv) y [aquí también](https://caelum-online-public.s3.amazonaws.com/1887-oracle-analytics-visualizando-datos/facturacion_semanal.csv).

### **Top 3 clientes**

El sector de marketing del **Club del Libro** está promoviendo una acción que premia a los 3 clientes que compraron más libros y artículos en la empresa, con una caja de libros más leídos al año. Para ello, Penélope necesita ayudar a identificar quiénes son estas personas y presentar una visualización que destaque los(as) 3 ganadores(as) y así divulgarlo en su página web.

De acuerdo con lo que aprendimos en el aula, ¿cómo podemos generar esta visualización destacando a los 3 mejores clientes?

Rta.

Luego de generar el gráfico de barras, debemos ir a la pestaña Mis cálculos, escoger un nombre para nuestra visualización, como por ejemplo TOP 3 Clientes, emplear la función TOPN(Valor de compra, 3), validar el cálculo y colocarlo en el eje deseado. La función TOPN crea una especie de clasificación de acuerdo con la columna que introducimos y la cantidad de valores que queremos presentar en nuestra visualización de datos.

### **Gráfico de línea y tendencia**

En el aula fue citado que, cuando se trata de series temporales, lo más adecuado es utilizar gráficos de líneas. Además, es posible insertar líneas de tendencia y otros elementos que pueden enriquecer nuestro análisis de los datos.

Así, indica las alternativas que citan las razones para utilizar este tipo de visualización y elementos en nuestros análisis:

Rta.

- Las líneas de tendencia en un gráfico de líneas nos ayudan a tener un panorama general de cómo nuestros datos evolucionan en un tiempo determinado. A través de estas, es posible notar que, aunque existan caídas y elevaciones sucesivas, nuestra facturación tiende a subir o a bajar de acuerdo con el lapso en cuestión. Como el propio nombre lo indica, una línea de tendencia busca demostrar hacia dónde se dirigen nuestros datos de acuerdo con un periodo de tiempo determinado. Es posible interpretar y tratar de predecir si nuestra inversión o facturación aumenta o disminuye durante dicho espacio de tiempo.
- Un gráfico de líneas es el más adecuado, pues logramos visualizar la evolución de nuestros datos de acuerdo con el tiempo que determinamos, siendo más fácil evidenciar caídas o elevaciones de los valores que estamos analizando. Un gráfico de líneas realmente facilita la visualización de la variación de los datos a lo largo de determinado tiempo, pues tenemos el hábito de, al depararnos con un gráfico de ese estilo, tratar de acompañar la línea con los ojos o los dedos.

### **¿Cuál es el gráfico más adecuado?**

La coordinadora del proyecto solicitó, en el análisis de Penélope, la creación de una visualización de datos que representase las ventas de acuerdo con los Estados.

¿Cuál es la visualización que atendería mejor las necesidades de la coordinadora?

Rta.

![¿Cual es el grafico mas adecuado?](./img/cual-es-el-grafico-mas-adecuado.jpg)

El mapa de calor es la visualización que favorece la percepción de valores que queremos destacar para nuestra clientela. Así, logramos, a través de la tonalidad de los colores, identificar los Departamentos (Entiéndase Estados y/o Provincias) que más vendieron.

### **Para saber más: mapas de calor**

En el aula anterior, aprendimos un poco sobre el uso de mapas a través del análisis de facturación en relación a los departamentos de los clientes del Club del Libro. Para profundizar un poco más sobre mapas, sobre todo el **mapa de calor** ampliamente utilizado en visualización de datos, vamos a considerar una situación hipotética.

**Construyendo el mapa de calor**

Para aprender a manipular y generar mapas de calor en **Oracle Analytics**, exploremos el siguiente ejemplo: Charlene es una analista de datos en una e-commerce de dulces llamada Candy Candy. Ella quedó a cargo de analizar los datos de facturación por mes de la empresa, en 2021, y necesitaba presentar sus datos en un mapa de calor por solicitud de su jefe.

Los datos recolectados con la debida facturación por mes fueron presentados de la siguiente forma:

| Fecha   | 2021     |
| ------- | -------- |
| 01/2021 | 1900,00  |
| 02/2021 | 84408,00 |
| 03/2021 | -9643,00 |
| 04/2021 | 49358,00 |
| 05/2021 | 10159,00 |
| 06/2021 | 19825,00 |
| 07/2021 | -3587,00 |
| 08/2021 | 51294,00 |
| 09/2021 | 73968,00 |
| 10/2021 | 14015,00 |
| 11/2021 | 91735,00 |
| 12/2021 | -4350,00 |

Para poder destacar los datos, sin necesitar adicionar muchos recursos y dejar la tabla más fácil para leer, podemos transformarla en un mapa de calor en el software de **Oracle Analytics**. Así, podremos seguir los siguientes pasos:

**Creando el Conjunto de Datos “Ventas”**

Antes de comenzar, debes copiar los datos de la tabla en un archivo de texto o un software de planillas y almacenarlo con el formato .csv con el nombre que desees aunque te sugiero usar el nombre candy_candy.csv . A continuación, crearemos un conjunto de datos navegando hasta el botón **Crear**, en la esquina superior derecha de la pantalla inicial, y seleccionamos la opción **Conjunto de Datos**.

![Mapas de color 1](./img/mapas-de-color-1.jpg)

Se abrirá el cuadro de diálogo a donde debemos arrastrar el archivo candy_candy.csv hasta nuestra ventana, o hacer clic sobre el botón upload para cargar nuestros datos en **Oracle Analytics**.

![Mapas de color 2](./img/mapas-de-color-2.jpg)

Luego de cargar los datos, una ventana abre y en ella se genera un nuevo conjunto de datos. En este punto, sin hacer modificaciones aún, debemos hacer clic en el botón **Adicionar** (en la esquina superior derecha) para crear nuestro conjunto de datos.

![Mapas de color 3](./img/mapas-de-color-3.jpg)

**Ajustando a columna Fecha para medida de tipo Date**

Ahora, somos redireccionados al conjunto de datos **candy_candy**. Observamos dos columnas en nuestros datos: **Fecha** está representada como un atributo; y **2021**, que contiene la facturación de Candy Candy, como medida.

Para que podamos reconocer la columna **Fecha** correctamente como medida de tiempo, necesitamos convertirla de atributo a tipo **Date**. Así, basta hacer clic con el botón derecho sobre la columna **Fecha** y escoger la opción **Convertir a Fecha**.

![Mapas de color 4](./img/mapas-de-color-4.jpg)

Después, necesitamos identificar el formato de origen ideal para la fecha. En nuestro conjunto de datos tenemos mes/año, o sea, **MM/yyyy**. Al definir esta opción, vamos a hacer clic en **Aplicar Script** en la pestaña lateral izquierda de la ventana para guardar los cambios.

![Mapas de color 5](./img/mapas-de-color-5.jpg)

Al haber transformado la columna **Fecha** en una medida de tipo **Date**, podemos extraer lapsos temporales. Para ello, crearemos una nueva columna haciendo clic sobre los 3 puntitos de la columna **Fecha** y seleccionamos las opciones de **Extraer → Mes del Año**.

![Mapas de color 6](./img/mapas-de-color-6.jpg)

El resultado será semejante al que podemos observar en la siguiente imagen. Nuestro conjunto de datos **candy_candy** ahora posee 3 columnas (Fecha, Fecha mes del Año 1, 2021).

![Mapas de color 7](./img/mapas-de-color-7.jpg)

Vamos a renombrar la columna creada como **Mes**. Para hacerlo, haremos clic sobre los 3 puntitos de la columna **Fecha Mes del Año 1** y escoger la opción Renombrar. Como mencionado previamente, llamaremos la columna **Mes**.

![Mapas de color 8](./img/mapas-de-color-8.jpg)

Para finalizar esta etapa y guardar todos los cambios vamos a hacer clic en **Aplicar Script** y, posteriormente, en **Crear Directorio de Trabajo** para utilizar la base de datos modificada.

**Creando el mapa de calor** Para crear nuestro mapa de calor, vamos a hacer clic sostenido y arrastrar la columna **Mes** hasta el espacio **Soltar Visualizaciones o Datos aquí**.

![Mapas de color 9](./img/mapas-de-color-9.jpg)

Esta ación genera una visualización automática de **Tabla**, que cambiaremos para **Tabla Dinámica**. Esto lo haremos haciendo clic sobre **Visualización Automática** y escogiendo la opción correspondiente a **Tabla Dinámica**, como consta en la siguiente imagen:

![Mapas de color 10](./img/mapas-de-color-10.jpg)

Para exhibir la cantidad de ventas por mes, basta arrastrar la columna 2021 a la pestaña **Valores** en el panel de gramática.

![Mapas de color 11](./img/mapas-de-color-11.jpg)

En este punto, adicionamos la intensidad de las ventas de 2021 arrastrando la columna 2021 a la pestaña **Color**. Nota que los colores de las celdas de la Columna **2021** fueron determinados, con diferentes tonalidades de azul, de acuerdo con los valores e iniciando desde los valores negativos (tono azul-claro, con el mínimo de -10k ) y para los valores positivos (tono azul-oscuro, con el máximo de 92k).

![Mapas de color 12](./img/mapas-de-color-12.jpg)

Para ajustar nuestro mapa de calor con colores que identifiquen mejor la idea de valores muy negativos, negativos, positivos y muy positivos, tomaremos como punto de partida la siguiente idea:

    Tonos de rojo (muy negativos) y naranja (negativos) para los meses en que hubo pérdidas.
    Tonos de verde (muy positivos) y amarillo (positivos) para los meses en que hubo lucro.

Para ello, es necesario hacer clic en la esquina superior derecha de la pestaña **Color** que abrirá un menu con la opción **Administrar Designaciones**.

![Mapas de color 13](./img/mapas-de-color-13.jpg)

Para alterar nuestra paleta de colores, haremos clic sobre la flecha al lado de la paleta de colores azules y localizaremos la que tiene el gradiente de tonos rojos y verdes, como en la imagen a continuación:

![Mapas de color 14](./img/mapas-de-color-14.jpg)

Al hacer clic en **Concluído** los colores de nuestra tabla cambiarán, y quedará así:

![Mapas de color 15](./img/mapas-de-color-15.jpg)

Para lograr adicionar el efecto como lo definimos anteriormente, necesitamos volver a **Administrar Designaciones**, y haremos clic en el cuadrado para configurar el color central de la paleta de colores y cambiarlo para un color de amarillo a naranja. Nuestra paleta de colores debe quedar semejante a la siguiente:

![Mapas de color 16](./img/mapas-de-color-16.jpg)

![Mapas de color 17](./img/mapas-de-color-17.jpg)

¡Felicitaciones! Has logrado elaborar el mapa de calor a través del recurso de **Tabla Dinámica**.

![Mapas de color 18](./img/mapas-de-color-18.jpg)

### **Lo que aprendimos**

- Utilizar las funciones de cálculo y conteo en la pestaña Mis cálculos;
- Seleccionar los datos a través de un ranking mediante la función TOPN;
- Adicionar un filtro para los datos;
- Generar gráficos de líneas para series temporales;
- Adicionar líneas de tendencia en series temporales;
- Crear mapas para el análisis de la distribución de los datos de acuerdo con las ciudades.

---

## **Elaborando la presentación**

### **Layout del Canvas**

Al utilizar el canvas de Oracle Analytics, Penélope percibió que las visualizaciones estaban siendo ajustadas automáticamente, pero a ella le gustaría realizar algunas alteraciones de forma libre. ¿Cómo puede realizar esta configuración del layout del canvas?

Rta.

- En la parte inferior del canvas, ella puede hacer clic con el botón derecho en el nombre de la pantalla. En las opciones disponibles, seleccionar Propiedades de pantalla y, en Layout, escoger la opción Formato Libre. Estos son los pasos que Penélope debe seguir para realizar la configuración necesaria y utilizar el Formato Libre.

### **COUNT y COUNT DISTINCT**

En el proyecto del dashboard del **Club del Libro**, utilizamos las funciones de agregación: COUNT(<columna> ) (conteo) para descubrir el número de clientes existentes en nuestra base de datos; y COUNT( DISTINCT <columna>) (conteo distinto) para contar el número de ciudades donde existen clientes registrados que realizan sus compras.

Sobre estas dos funciones y sus diferentes formas de utilización, indica la alternativa que describe correctamente cada una de ellas.

Rta.

**Count** realiza un conteo de todos los valores existentes en la columna. Ya **Count Distinct** realiza un conteo considerando tan solo una ocurrencia de cada valor. La función de agregación Count va a contar todos los valores presentes en la columna, mientras que Count Distinct contará solamente el número de valores diferentes entre sí.

### **Para saber más: importando visualizaciones externas**

Oracle posee una biblioteca externa en la cual encontramos varias visualizaciones diferentes que podemos importar en **Oracle Analytics** y así poder enriquecer aún más nuestros proyectos. Para realizar el download de una visualización externa, necesitamos acceder a la página de [Oracle Analytics Library](https://www.oracle.com/br/business-analytics/data-visualization/extensions/). En seguida, localizamos la extensión que deseamos importar en **Oracle Analytics** (por ejemplo la de **Mobile Grid Bars Plug-in**) y hacemos clic en **Download**.

!["Importando visualizaciones externas 1"](./img/importando-visualizaciones-externas-1.jpg)

Una caja de texto va a aparecer solicitando la confirmación de los términos de la licencia. y seleccionamos la opción **Accept License Agreement**, e iniciar sesión en tu cuenta de Oracle, para continuar con el download.

Un archivo de extensión .zip será descargado, la recomendación es que lo almacenes en el directorio de tu preferencia.

!["Importando visualizaciones externas 2"](./img/importando-visualizaciones-externas-2.jpg)

Luego de que finalice la descarga, abrimos **Oracle Analytics** en el menú sándwich, en la página inicial, seleccionamos la opción **Consola**.

!["Importando visualizaciones externas 3"](./img/importando-visualizaciones-externas-3.jpg)

Al hacer clic en **Consola** aparecerá el campo **Visualizaciones y Compartimiento**. Seleccionaremos la opción **Extensiones** y se abrirá una nueva página donde seleccionaremos **Subir Extensión**.

!["Importando visualizaciones externas 4"](./img/importando-visualizaciones-externas-4.jpg)

!["Importando visualizaciones externas 5"](./img/importando-visualizaciones-externas-5.jpg)

Localizamos el archivo en el directorio de descarga y el mismo quedará disponible en extensiones.

!["Importando visualizaciones externas 6"](./img/importando-visualizaciones-externas-6.jpg)

Para poder tener acceso a esta visualización en tu proyecto, será necesario reiniciar **Oracle Analytics**. Las nuevas visualizaciones importadas estarán disponibles en visualizaciones, en la opción de Visualizaciones Personalizadas.

!["Importando visualizaciones externas 7"](./img/importando-visualizaciones-externas-7.jpg)

### **Para saber más: profundizando en el Storytelling**

**Storytelling es el arte de contar historias utilizando técnicas inspiradas en libretistas y escritores para transmitir un mensaje de forma fluida e inolvidable.** Cuando desarrollamos un proyecto para hacer una presentación, debemos tener una buena narrativa detrás de aquel contenido que pueda enriquecer su sentido y haga que las personas comprendan todo el proceso de forma objetiva.

El Storytelling es considerado uno de los **soft skills** más importantes en el área de datos, pues complementa y finaliza un proyecto sujetando toda la información a una línea continua.

### **Haga lo que hicimos en aula**

¿Qué tal si te propongo un desafío? utiliza una visualización externa disponible para download en **Oracle Analytics library** para aplicar en nuestro proyecto del Club del Libro.

Para ello, accesa **Oracle Analytics library**, escoge una visualización y realiza el download. Luego realiza el **Oracle Analytics** como fue demostrado en la actividad **Importando visualizaciones externas**.

### **Lo que aprendimos**

- Identificar las funcionalidades de Layout del Canvas;
- Las diferencias entre las funciones de agregación **COUNT** y **COUNT DISTINCT**;
- Trabajar con imágenes externas;
- Utilizar el área de presentación;
- Definir _Storytelling_.
