# **Data Visualization - Explorando con Seaborn**

## **Importando y traduciendo variables**

### **Preparando el ambiente**

Bienvenidos y bienvenidas al curso Data Visualization: Explorando con Seaborn

Yo soy el instructor Mauricio Loaiza y en este curso haremos análisis de datos utilizando la biblioteca Pandas y análisis visual y de gráficos con la biblioteca Seaborn, que Python nos ofrece.

En este [link](https://github.com/alura-es-cursos/1779-Data-Visualization-Explorando-con-Seaborn/tree/aula1 "Aula1") puedes encontrar la base con la que estaremos trabajando a lo largo de este curso. O si deseas puedes descargar el archivo zip haciendo [click aquí](https://github.com/alura-es-cursos/1779-Data-Visualization-Explorando-con-Seaborn/archive/refs/heads/aula1.zip "Aula1.zip").4

**Ambiente de Análisis**

En este curso usaremos la herramienta Colaboratory o Google Colab que Google nos ofrece de forma gratuita. Esta herramienta es un Jupyter notebook que no requiere ninguna configuración.

**Colaboratory**

Para usar este ambiente es necesario tener una cuenta en google, dado que los notebooks que son creados en Colaboratory quedarán guardados en Google Drive.

En este link puedes crear una cuenta de Google, caso no la tengas: [Crea una cuenta de Google](https://accounts.google.com/signup/v2/webcreateaccount?flowName=GlifWebSignIn&flowEntry=SignUp "Gmail").

**Informaciones adicionales sobre Colaboratory**

- El código de tu notebook es ejecutado en una máquina virtual dedicada a tu cuenta. Las máquinas virtuales son recicladas por un determinado tiempo.
- Cada vez que abramos nuestro notebook, será necesario hacer upload del archivo que queramos analizar.

**¿Puedo usar un ambiente diferente a Google Colaboratory?**

Si, no habría ningún problema. Una opción sería [Anaconda](https://www.anaconda.com/download "Download").

Los espero en la siguiente clase.

### **Leyendo un archivo CSV**

Una persona trabajando como analista de datos recibe una base de datos llamada precios.csv para hacer una análisis de las informaciones contenidas en ella.

¿Cómo debe proceder esta persona para importar y guardar la base de datos con el nombre datos?

Rta.

```py
import pandas as pd
datos = pd.read_csv(‘precios.csv’)
```

Primero importamos pandas y después importamos y guardamos nuestra base con el nombre datos usando la función _read_csv_ de pandas.

### **Traducción de variables**

Una persona creó un notebook para analizar la población de algunos países europeos, las columnas y los campos están en inglés.

Antes de comenzar el análisis, esta persona decide traducir los datos que están en inglés al español, así como se muestra en la imagen abajo:

![Traduccion de variables](./img/traduccion-de-variables.png "Traduccion de variables")

¿Qué opción le recomendarias que use para hacer esta traducción?

Rta.

- Para traducir los nombres de las columnas la persona podría usar el siguiente código:

```py
columnas_nuevas = {
    'Country': 'País',
    'Population': 'Población'
}

datos = datos.rename(columns = columnas_nuevas)
datos
```

Para traducir los nombres de las columnas debemos usar la opción rename y pasamos el diccionario que creamos con la llave y el valor traducido.

- Para traducir los campos en la columna de Country, la persona podría usar el siguiente código:

```py
nombre_paises = {
    'Germany': 'Alemania',
    'France': 'Francia',
    'Spain': 'España'
}

datos.Country = datos.Country.map(nombre_paises)
datos
```

Para traducir los campos en la columna debemos asignar la columna, debemos usar la función .map copiando el diccionario con la llave y el valor traducido.

### **Haga lo que hicimos en aula**

Como informamos anteriormente, en este curso trabajaremos con las bibliotecas de Pandas y Seaborn.

Comenzamos importando la biblioteca Pandas para poder traer la base que usaremos en el curso

```py
import pandas as pd
```

Después usamos pandas para leer la base y guardar como un DataFrame con el nombre de datos.

Así como mencionamos en el video 1.2, debemos subir la base de datos a Google Colaboratory caso estés usando esta herramienta:

```py
datos = pd.read_csv(‘credit_card.csv’)
```

Podemos hacer una visualización inicial de nuestra base de datos usando la función head() e info(). La primera nos permite ver, por defecto, el nombre de las columnas y las primeras 5 líneas de cada una de ellas:

```py
datos.head()
```

La segunda nos muestra las características de las variables en nuestra base de datos, la cantidad de líneas en cada una de ellas y el tipo de variable, si es un número entero o decimal, o si es una variable de texto.

```py
datos.info()
```

Por motivos didácticos decidimos traducir al español el nombre de las columnas y los contenidos dentro de ellas dado que la base originalmente está en inglés.

Primero seleccionamos las columnas,

```py
datos.columns
```

Enseguida creamos un Diccionario con el nombre de las columnas y el nombre que queremos para ellas:

```py
dic_columnas = {
'LIMIT_BAL' : 'limite',
'CHECKING_ACCOUNT' : 'cuenta_corriente',
'EDUCATION' : 'escolaridad',
'MARRIAGE' : 'estado_civil',
'AGE' : 'edad',
'BILL_AMT' : 'valor_factura',
'PAY_AMT' : 'valor_pago',
'DEFAULT' : 'moroso'
}
```

Después de crear nuestro Diccionario, usamos la función rename y en este momento creamos un nuevo DataFrame (tarjetas) ya con el nombre de nuestras columnas traducidas.

```py
tarjetas = datos.rename(columns = dic_columnas)
```

Ya tenemos el nombre de nuestras columnas en español, ahora debemos traducir los campos de nuestras columnas de texto en la base.

**Variable cuenta corriente**

Comenzaremos con la variable cuenta_corriente. Primero identificamos los campos que tenemos en esta columna con la función unique().

```py
tarjetas.cuenta_corriente.unique()
```

Después crearemos un nuevo diccionario con los campos identificados y con el nombre que queramos darle a estos campos.

```py
dic_cuenta = {
'Yes' : 'Si',
'No' : 'No'
}
```

Y finalmente usamos el diccionario creado para actualizar los campos en la base de datos.

En el lado izquierdo de la función está el nombre de la base datos y la columna que queremos alterar. En la parte derecha seleccionamos de nuevo la base de datos, la columna que vamos a ajustar y la función map() con el diccionario como parámetro.

```py
tarjetas.cuenta_corriente = tarjetas.cuenta_corriente.map(dic_cuenta)
```

Haremos un proceso similar para las demás variables que están en formato texto.

**Variable escolaridad**

```py
tarjetas.escolaridad.unique()
dic_escolaridad = {
'2.University' : '2.Universidad',
'3.Graduate School' : '3.Pos-graduación',
'1.High School' : '1.Colegio'
}
tarjetas.escolaridad = tarjetas.escolaridad.map(dic_escolaridad)
```

**Variable estado civil**

```py
tarjetas.estado_civil.unique()
dic_estado_civil = {
'Married' : 'Casado/a',
'Single' : 'Soltero/a'
}
tarjetas.estado_civil = tarjetas.estado_civil.map(dic_estado_civil)
```

**Lo que aprendimos**

- Hacer upload de archivo csv para Google Colab. Importamos pandas e importamos la base de datos con la que vamos a trabajar en el curso: credit_card.csv.
- Conocimos nuestra base de datos y las variables que vamos a analizar en clase.
- Por cuestiones didácticas decidimos hacer la traducción al español de las variables categóricas en la base que se encontraban en inglés.
- Guardamos nuestra base de datos con el nombre de tarjetas.

---

## **Análisis exploratoria de variables**

### **Creando un gráfico de distribución**

Asumiendo que hemos importado nuestra base de datos, como podemos crear un gráfico de distribución de la variable _valor_pago_ usando la función _displot_ de Seaborn.

Rta.

```py
sns.displot(data=tarjetas, x='valor_pago');
```

Haz llamado correctamente la biblioteca de Seaborn y el gráficos que queremos mostrar, así como la base de datos y el campo que será la variable x.

### **Insertando nuevos campos**

Una empresa registra las informaciones de productos vendidos por sus vendedores en un [notebook](https://github.com/alura-es-cursos/1779-Data-Visualization-Explorando-con-Seaborn/blob/aula2/Aula2_Nro4_Insertando_nuevos_campos.ipynb "GitHub") para mantener un registro histórico del desempeño de los funcionarios.

Se decidió incluir una columna con el promedio de ventas que los vendedores han tenido en los últimos 3 días:

![Insertando nuevos campos](./img/insertando-nuevos-campos.png "Insertando nuevos campos")

¿Cuál código se puede usar para crear esta columna de Promedio, dejando el valor calculado con dos números decimales?

Rta.

```py
base['Promedio'] = ((base['Dia_1']+base['Dia_2']+base['Dia_3'])/3).round(2)
```

Estamos creando la columna Promedio, y el cálculo se está realizando de la manera correcta, usamos la función _round()_ para dejar dos cifras decimales.

### **Haga lo que hicimos en aula**

Para esta aula y para las siguientes aulas usaremos Seaborn, una biblioteca para análisis visual de datos. Para hacer la instalación puedes usar pip, así como está descrito abajo.

Si estás usando Google Colaboratory, la biblioteca ya está instalada.

```py
!pip install seaborn
```

Usaremos la versión 0.11

Para importar Seaborn en nuestro notebook lo hacemos de la siguiente forma:

```py
import seaborn as sns
```

Usamos sns por convención.

Vamos a comenzar mostrando un ejemplo de las imágenes que podemos crear con Seaborn usando la función _displot_.

Esta función nos permite ver la función de una variable:

```py
sns.displot(data=tarjetas, x='limite', hue='escolaridad')
```

**data**: es nuestra base de datos
**x**: es la variable numérica que vamos a analizar.

**Creación de variable**

Ahora crearemos una nueva variable en nuestra base de datos, esto para profundizar en el entendimiento de nuestra variables y poder crear relaciones entre ellas. Crearemos el Índice de Uso (IU) que es el porcentaje que usamos del total de límite disponible que tenemos.

```py
tarjetas['iu'] = tarjetas['valor_factura'] / tarjetas['limite']
```

Y generamos nuevamente un gráfico de distribución para saber cómo se comporta nuestra variable con los clientes que tenemos en la base de datos:

```py
sns.displot(data=tarjetas, x='iu')
```

**Algunas características de Seaborn**

Seaborn es una biblioteca que usa la biblioteca Matplotlib para realizar todos sus procesos. Dado este hecho, algunas características de Matplotlib están disponibles para nosotros en Seaborn, por ejemplo, podemos usar las opciones de colores que Matplotlib nos ofrece para personalizar nuestros gráficos de la forma que queramos.

En este [link](https://matplotlib.org/3.1.0/tutorials/colors/colormaps.html "Matplot") podemos conocer todas las opciones de colores que tenemos disponibles.

Para hacer uso de esta usamos el parámetro _palette_ en nuestras funciones de Seaborn.

Como ejemplo:

```py
sns.displot(data=tarjetas, x='limite', hue='escolaridad', palette='inferno_r');
```

Seaborn también nos permite dar algunos estilo a nuestros gráficos, para hacer uso de esto, podemos usar la función _set_style()_

Solo debemos ejecutarla una vez y todos los gráficos que generemos después de la ejecución ya estarán parametrizados, las opciones que tenemos son: _(darkgrid, whitegrid, dark, white, ticks)_, ejemplo:

```py
sns.set_style(‘darkgrid’)
```

### **Lo que aprendimos**

- Importamos Seaborn para analizar de forma gráfica las variables que tenemos en nuestra base de datos.
- Construimos una nueva variable numérica en nuestra base de datos.
- Creamos un histograma para analizar variables en nuestra base de datos.
- Descubrimos los estilos y colores que Seaborn nos ofrece.

---

## **Comparando y analizando variables categóricas**

### **Generando gráficos con Seaborn**

Queremos estudiar algunas variables en nuestra base de datos de tarjetas de crédito, para eso utilizaremos Seaborn, de esta forma podemos hacer un análisis visual de los datos.

Observa los gráficos abajo y marca las respuestas verdaderas:

![Generando graficos con Seaborn](./img/generando-graficos-con-seaborn.png "Generando graficos con Seaborn")

Rta.

- Para generar el tercer gráfico, usamos el código:

```py
sns.catplot(x='escolaridad', y='limite', data=tarjetas)
```

Si quieres saber más de Catplot, puedes acceder a este site: [Catplot](https://seaborn.pydata.org/generated/seaborn.catplot.html?highlight=catplot#seaborn.catplot "Catplot").

- Para generar el primer gráfico, usamos el código:

```py
sns.countplot(x='cuenta_corriente', data= tarjetas)
```

Si quieres saber más de Countplot, puedes acceder a este site: [Countplot](https://seaborn.pydata.org/generated/seaborn.countplot.html "Countplot").

- Para generar el segundo gráfico, usamos el código:

```py
sns.swarmplot(x='escolaridad', y='iu', data= tarjetas)
```

Si quieres saber más de Swarmplot, puedes acceder a este site: [Swarmplot](https://seaborn.pydata.org/generated/seaborn.swarmplot.html?highlight=swarmplot#seaborn.swarmplot "Swarmplot").

### **Agrupando el índice de uso**

Una persona analiza nuestra base de datos y considera que deberíamos agrupar la columna de iu, porque considera que más adelante podríamos cruzarla con otras informaciones de forma más eficiente.

Nos recomienda separarla en 4 grupos, como podríamos hacerlo:

Rta.

```py
bins = [0, 0.25, 0.50, 0.75, 1]
nombres = ['0%-25%', '25%-50%', '50%-75%', '75%-100%']

tarjetas['rango_iu'] = pd.cut(tarjetas['iu'], bins, labels=nombres)
```

Primero creamos una lista con los valores que serán nuestro punto de corte. Después creamos una lista con los nombres que le daremos a nuestros rangos y por últimos usaremos la función _cut_ de pandas para crear nuestra variable en la base de datos.

### **Haga lo que hicimos en aula**

En esta aula conocemos algunos gráficos que nos permiten analizar fácilmente variables categóricas o de texto.

**Countplot**

Este gráfico nos permite contar el número de vezes que una opción se repite en una variable, por ejemplo, la variable Cuenta corriente tiene las opciones Si y No, con countplot podemos contar el numero de vezes que cada una está en la base.

```py
sns.countplot(x='cuenta_corriente', data= tarjetas)
```

No sólo eso, con el parámetro hue de esta función podemos traer otra variable para nuestro gráfico y analizarla al lado de nuestra variable x. Podemos aprovechar para usar el parámetro palette y personalizar un poco nuestro gráfico.

```py
sns.countplot(x='cuenta_corriente', data= tarjetas, hue='moroso', palette='coolwarm')
```

**Catplot**

Este gráfico nos permite comparar una variable categórica (eje x) y una variable numérica (eje y). Podemos ver cómo está distribuida nuestra variable categórica en relación con nuestra variable numérica:

```py
sns.catplot(x='estado_civil', y='limite', data=tarjetas)
```

De la misma forma podemos usar el parámetro hue, para traer otra variable a nuestro gráfico, también usamos el parámetro dodge para que se muestran columnas diferentes para esta variable y no se agrupen solamente en una columna.

```py
sns.catplot(x='estado_civil', y='limite', data=tarjetas, hue='moroso', dodge=True)
```

**Swarmplot**

Es un gráfico bastante parecido al catplot solo que en este gráfico los puntos de nuestra variable no se sobreponen. Por este detalle, cuando la base de datos es muy grande es posible que algunos datos se pierdan.

```py
sns.swarmplot(x='escolaridad', y='iu', data=tarjetas)
```

**Boxplot**

En este gráfico analizamos también una variable categórica y una variable numérica, nos permite observar más información de nuestra variable analizada. Aquí podemos usar también algunos parámetros como hue o palette.

```py
sns.boxplot(x='escolaridad', y='iu', data=tarjetas, hue='moroso', palette='bone')
```

Comencemos viendo el rectángulo, la línea inferior es el percentil 25%, la línea superior es el percentil 75% y la línea que cruza el rectángulo es el percentil 50% o la mediana.

Hay dos líneas perpendiculares que van más allá de las medidas del rectángulo, el punto inferior de la línea que va hacia abajo es el punto mínimo en nuestra variable, y el punto máximo de la línea que va hacia arriba es el punto máximo.

Si hay puntos más allá de estos dos líneas, estos son considerados outliers.

**Violinplot**

Este gráfico es muy parecido al boxplot, sólo que en este caso podemos ver la distribución de forma vertical en cada parámetro de nuestro gráfico.

```py
sns.violinplot(x='escolaridad', y='iu', data=tarjetas, hue='moroso')
```

**Creación de variable categórica a partir de una variable numérica**

Vamos a analizar ahora una variable numérica, la edad, como podemos deducir, en esta variable tenemos muchos valores, desde 18 hasta 75 años (edad máxima en nuestra base de datos), para facilitar el análisis vamos a agruparlas en grupos. Haremos esto usando la función cut de pandas.

Podemos saber las edades que tenemos en nuestra base de datos usando la siguiente fórmula:

```py
tarjetas.edad.unique()
```

Crearemos 4 rangos usando el siguiente código:

```py
bins = [18, 30, 40, 50, 100]
nombres = ['18-30', '30-40', '40-50', '50+']
tarjetas['rango_edad'] = pd.cut(tarjetas['edad'], bins, labels=nombres)
```

Los valores en la lista bins se usan como los límites mínimos y máximos de nuestros rangos. Lo que tenemos en nuestra lista nombres son los nombres de nuestros rangos, y en la tercera línea usamos la función cut para crear nuestra variable en la base de datos con el nombre rango_edad.

Podemos analizarla inicialmente con un boxplot para ver su distribución inicial:

```py
sns.boxplot(x='rango_edad', y='limite', data= tarjetas);
```

Lo que aprendimos
Próxima Actividad

### **Lo que aprendimos en esta aula:**

- Aprendimos a crear diferentes gráficos que nos ayudan a analizar variables categóricas: **countplot**, **catplot**, **swarmplot**.
- Aprendimos a usar el parámetro hue, que nos sirve para fraccionar una variable en nuestro gráfico.
- Introducimos los gráficos **boxplot** y **violinplot**.
- Creamos y analizamos con boxplot la variable categórica rango_edad.

---

## **Análisis de variables numéricas y regresión**

### **Analizando distribuciones**

Una persona que está analizando la base de datos de tarjetas de crédito genera este gráfico para ver cómo está distribuida la variable limite:

![Analizando distribuciones](./img/analizando-distribuciones-1.png "Analizando distribuciones")

Sin embargo, esta persona quiere agregar un parámetro para descubrir si la distribución de las personas que tienen cuenta corriente es diferente a la distribución de las personas que no tienen, ¿será que es posible?

¿Cómo podría determinar esto?

Rta.

- Es posible, al utilizar displot() podemos usar el parámetro hue para mostrar diferentes distribuciones en un mismo gráfico.

![Analizando distribuciones](./img/analizando-distribuciones-2.png "Analizando distribuciones")

Con este parámetro podemos mostrar dos distribuciones, una para los clientes con cuenta corriente y otra para los que no tienen:

- Es posible, al utilizar displot() podemos usar el parámetro col para abrir diferentes gráficos, uno para cada campo en nuestra variable.

![Analizando distribuciones](./img/analizando-distribuciones-3.png "Analizando distribuciones")

El parámetro col creará 2 gráficos, uno con la distribución de límite para los clientes con cuenta corriente y otro para los clientes sin cuenta corriente:

### **Sobre el test de hipótesis**

En nuestro proyecto realizamos un test de hipótesis buscando entender cómo se relaciona la variable del valor de factura con la variable de moroso y no moroso. Nuestro p-value fue el siguiente:

```py
p-value: 0.010391243081223555
```

¿Qué representa este valor?

Rta.

La distribución del valor de factura entre los grupos moroso y no moroso no es igual, por lo que podemos aceptar la hipótesis alternativa. Dado que nuestro p-value es menor a 0.05 podemos entender que nuestro resultado es estadísticamente significativo y podemos aceptar la hipótesis alternativa.

### **Haga lo que hicimos en aula**

**Gráfico de distribución**

En esta aula haremos el análisis de variables numéricas, comenzaremos profundizando en el gráfico de distribución del que ya hablamos anteriormente.

En esta función también podemos usar algunos parámetros para hacer comparaciones de variables.

```py
sns.displot(data=tarjetas, x='limite', col='escolaridad', kind='kde');
```

Con el parámetro col, la función nos abre un gráfico para cada campo dentro de la variable que estamos analizando.

Con el parámetro hue, podemos ver várias distribuciones dentro de un mismo gráfico, cada campo de la variable tendrá una distribución diferente:

```py
sns.displot(data=tarjetas, x='limite', kind='kde', hue='rango_edad');
```

**Gráfico de dispersión y línea de regresión**

El gráfico de dispersión nos muestra cómo se relacionan dos variables numéricas y si existe alguna tendencia o correlación entre ellas, la podemos usar de la siguiente forma:

```py
sns.scatterplot(x='iu', y='valor_factura', data=tarjetas);
```

En esta función podemos usar el parámetro hue, lo que nos permitirá abrir varias dispersiones en el gráfico, una para cada campo de nuestra variable.

```py
sns.scatterplot(x='iu', y='valor_factura', data=tarjetas, hue='cuenta_corriente');
```

La línea de dispersión la podemos ver usando la función lmplot (linear model plot), este gráfico nos mostrará también un gráfico de dispersión y la línea de regresión que mejor se ajuste a la relación entre las dos variables:

```py
sns.lmplot(x='iu', y='valor_factura', data=tarjetas);
```

**Test de hipótesis**

Ahora realizaremos un test de hipótesis con la variable moroso, para conocer si estos dos grupos (moroso y no moroso) son estadísticamente diferentes o no.

Comenzamos importando la biblioteca scipy de python, dado que la usaremos para calcular más adelante el p-value.

```py
from scipy.stats import ranksums
```

Nuestras hipótesis nula y alternativa serán respectivamente:

Hipótesis nula: _La distribución de los grupos moroso y no moroso es la misma_

Hipótesis alternativa: _La distribución de los grupos moroso y no moroso no es la misma_

Separamos los grupos moroso y no moroso usando el valor de la factura como base:

```py
moroso = tarjetas.query("moroso == 1").valor_factura
no_moroso = tarjetas.query("moroso == 0").valor_factura
```

Usamos la función ranksums de scipy para calcular el p-value:

```py
ranksums(moroso, no_moroso)
```

### **Lo que aprendimos**

- Hicimos un análisis más profundo de los gráficos de distribución (histograma) usando displot.
- Creamos gráficos diferentes, categorizando nuestra información con el parámetro col.
- Hicimos una introducción al gráfico de dispersión (**scatterplot**) y al gráfico de modelo linear (**lmplot**).
- Realizamos un test de hipótesis, para intentar determinar si la diferencia entre los grupos morosos y no morosos es estadísticamente significativa.

---

## **Análisis conjunta de variables**

### **Comparando la edad con el índice de uso**

En la última aula vimos que es posible generar de forma rápida gráficos que nos permitan visualizar al mismo tiempo como es la distribución individual de una variable y cómo se relaciona con otra variable con la que deseemos hacer comparaciones.

Nos han pedido interpretar las variables edad e índice de uso (_‘iu’_) en nuestra base de datos.

¿Qué código podemos utilizar para generar el gráfico a continuación?

![Comparando la edad con el índice de uso](./img/comparando-la-edad-con-el-%C3%ADndice-de-uso.png "Comparando la edad con el índice de uso")

Rta.

```py
sns.jointplot(x='edad', y='iu', data=tarjetas, kind='scatter')
```

Podemos usar el gráfico de jointplot para visualizar el histograma de cada variable de forma individual y al mismo tiempo podemos observar cómo se relacionan entre ellas. En esta línea de código estamos usando los parámetros necesários.

### **Entendiendo un Pairplot**

En la última aula vimos que si usamos Pairplot esta opción generará una serie de gráficos simultáneamente, tal como nos muestra el ejemplo a seguir:

```py
sns.pairplot(data = tarjetas)
```

Y nuestro resultado sería una série de gráficos de esta forma:

![Entendiendo un Pairplot](./img/entendiendo-un-pairplot.png "Entendiendo un Pairplot")

Teniendo en cuenta esto, ¿Cómo podemos definir un Pairplot?

Rta.

Una función que creará una cuadrícula de gráficos de forma que cada variable numérica será analizada con las demás variables numéricas de la base de datos y con ella misma.

Un **Pairplot** creará una columna y una fila para cada variable numérica presente en nuestra base de datos y hará un análisis de cada variable individualmente y como se relaciona con las demás variables numéricas de la base.

### **Haga lo que hicimos en aula**

**Jointplot**

El jointplot nos permite analizar al mismo tiempo la distribución de dos variables de forma individual y como se relacionan estas variables entres ellas. En este gráfico también podemos usar el parámetro hue para analizar nuestros gráficos a partir de otra variable.

```py
sns.jointplot(x='edad', y='limite', data=tarjetas, kind='kde', hue='moroso')
```

Podemos definir el tipo de gráfico que queremos mostrar con el parámetro kind, las opciones que éste parámetro nos permite son:

```py
(“scatter”, “kde”, “hist”, “hex”, “reg”, “resid” )
```

**Pairplot**

El pairplot nos permite visualizar con una línea de código cómo se relacionan todas las variables numéricas en nuestra base de datos, cuando la misma variable se cruza en la fila y la columna tendremos un gráfico de distribución, cuando se cruza una variable con las demás tendremos un gráfico de dispersión,

En este gráfico también podemos usar el parámetro hue para incluir otra variable categórica en el análisis de los gráficos o el parámetro palette para darle una personalización a nuestro gráfico.

```py
sns.pairplot(data=tarjetas, hue='escolaridad', palette='cividis')
```

### **Lo que aprendimos**

- Introducimos el gráfico **jointplot** con sus opciones: scatter, kde, hex. Esto para analizar la variable de edad y limite.
- Hicimos un análisis descriptivo de las variables en nuestra base de datos.
- Usamos el gráfico **pairplot** para hacer un análisis conjunto de todas nuestras variables numéricas en la base de datos.
